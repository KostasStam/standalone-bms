#include "common.h"


void common_under_threshold_process(int16_t reading, int16_t *thresholds, uint16_t *bucket_sizes, uint8_t *buckets)
{
	// Check for Permanent Fault--------------------------------------------------------
	if (reading < thresholds[PER_FAULT])
	{
	#ifdef DEBUG
		UART_Printf("\nUNDER PER FAULT: %d, %d", reading, thresholds[PER_FAULT]);
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Increase Permanent Fault-----------------------------------------------------
		if (buckets[PER_FAULT] < bucket_sizes[PER_FAULT])								//As long as permanent faults in bucket are under the limit
		{
			buckets[PER_FAULT]	+= 1;													//Add 1 to the permanent fault leaky bucket
		}
		//Increase Fault---------------------------------------------------------------
		if (buckets[FAULT] < bucket_sizes[FAULT])										//As long as faults in bucket are under the limit
		{
			buckets[FAULT]		+= 1;													//Add 1 to the fault leaky bucket
		}
		//Increase Warning-------------------------------------------------------------
		if (buckets[WARNING] < bucket_sizes[WARNING])									//As long as warnings in bucket are under the limit
		{
			buckets[WARNING]	+= 1;													//Add 1 to the warning leaky bucket
		}
	}
	// Check for Fault-----------------------------------------------------------------
	else if (reading < thresholds[FAULT])
	{
	#ifdef DEBUG
		UART_Printf("\nUNDER FAULT");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Increase Fault---------------------------------------------------------------
		if (buckets[FAULT] < bucket_sizes[FAULT])										//As long as faults in bucket are under the limit
		{
			buckets[FAULT]		+= 1;													//Add 1 to the fault leaky bucket
		}
		//Increase Warning-------------------------------------------------------------
		if (buckets[WARNING] < bucket_sizes[WARNING])									//As long as warnings in bucket are under the limit
		{
			buckets[WARNING]	+= 1;													//Add 1 to the warning leaky bucket
		}
	}	
	// Check for Warning---------------------------------------------------------------
	else if (reading < thresholds[WARNING])
	{		
	#ifdef DEBUG
		UART_Printf("\nUNDER WARNING");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Reduce Fault-----------------------------------------------------------------
		if (buckets[FAULT] > 0)															//If fault bucket is not empty
		{
			buckets[FAULT]		-= 1;													//Subtract 1 from the fault leaky bucket
		}
		//Increase Warning-------------------------------------------------------------
		if (buckets[WARNING] < bucket_sizes[WARNING])									//As long as warnings in bucket are under the limit
		{
			buckets[WARNING]	+= 1;													//Add 1 to the warning leaky bucket
		}
	}
	// Reduce warnings, faults, pFaults
	else if (reading > thresholds[RECOVER])
	{
	#ifdef DEBUG
		UART_Printf("\nOVER RECOVER");
	#endif
		//Increase Recover-------------------------------------------------------------
		if (buckets[RECOVER] < bucket_sizes[RECOVER])									//As long as recover in bucket are under the limit
		{
			buckets[RECOVER]	+= 1;													//Add 1 to the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Reduce Fault-----------------------------------------------------------------
		if (buckets[FAULT] > 0)															//If fault bucket is not empty
		{
			buckets[FAULT]		-= 1;													//Subtract 1 from the fault leaky bucket
		}
		//Reduce Warning---------------------------------------------------------------
		if (buckets[WARNING] > 0)														//If warning bucket is not empty
		{
			buckets[WARNING]	-= 1;													//Subtract 1 from the warning leaky bucket
		}
	}
	else
	{
	#ifdef DEBUG
		UART_Printf("\nOVER WARNING UNDER RECOVER");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Reduce Fault-----------------------------------------------------------------
		if (buckets[FAULT] > 0)															//If fault bucket is not empty
		{
			buckets[FAULT]		-= 1;													//Subtract 1 from the fault leaky bucket
		}
		//Reduce Warning---------------------------------------------------------------
		if (buckets[WARNING] > 0)														//If warning bucket is not empty
		{
			buckets[WARNING]	-= 1;													//Subtract 1 from the warning leaky bucket
		}		
	}
}



void common_over_threshold_process(int16_t reading, int16_t *thresholds, uint16_t *bucket_sizes, uint8_t *buckets)
{
	// Check for Permanent Fault--------------------------------------------------------
	if (reading > thresholds[PER_FAULT])
	{
	#ifdef DEBUG
		UART_Printf("\nOVER PER FAULT");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Increase Permanent Fault-----------------------------------------------------
		if (buckets[PER_FAULT] < bucket_sizes[PER_FAULT])								//As long as permanent faults in bucket are under the limit
		{
			buckets[PER_FAULT]	+= 1;													//Add 1 to the permanent fault leaky bucket
		}
		//Increase Fault---------------------------------------------------------------
		if (buckets[FAULT] < bucket_sizes[FAULT])										//As long as faults in bucket are under the limit
		{
			buckets[FAULT]		+= 1;													//Add 1 to the fault leaky bucket
		}
		//Increase Warning-------------------------------------------------------------
		if (buckets[WARNING] < bucket_sizes[WARNING])									//As long as warnings in bucket are under the limit
		{
			buckets[WARNING]	+= 1;													//Add 1 to the warning leaky bucket
		}
	}
	// Check for Fault-----------------------------------------------------------------
	else if (reading > thresholds[FAULT])
	{
	#ifdef DEBUG
		UART_Printf("\nOVER FAULT");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Increase Fault---------------------------------------------------------------
		if (buckets[FAULT] < bucket_sizes[FAULT])										//As long as faults in bucket are under the limit
		{
			buckets[FAULT]		+= 1;													//Add 1 to the fault leaky bucket
		}
		//Increase Warning-------------------------------------------------------------
		if (buckets[WARNING] < bucket_sizes[WARNING])									//As long as warnings in bucket are under the limit
		{
			buckets[WARNING]	+= 1;													//Add 1 to the warning leaky bucket
		}
	}
	// Check for Warning---------------------------------------------------------------
	else if (reading > thresholds[WARNING])
	{
	#ifdef DEBUG
		UART_Printf("\nOVER WARNING");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Reduce Fault-----------------------------------------------------------------
		if (buckets[FAULT] > 0)															//If fault bucket is not empty
		{
			buckets[FAULT]		-= 1;													//Subtract 1 from the fault leaky bucket
		}
		//Increase Warning-------------------------------------------------------------
		if (buckets[WARNING] < bucket_sizes[WARNING])									//As long as warnings in bucket are under the limit
		{
			buckets[WARNING]	+= 1;													//Add 1 to the warning leaky bucket
		}
	}
	// Reduce warnings, faults, pFaults
	else if (reading < thresholds[RECOVER])
	{
	#ifdef DEBUG	
		UART_Printf("\nUNDER RECOVER");		
	#endif
		//Increase Recover-------------------------------------------------------------
		if (buckets[RECOVER] < bucket_sizes[RECOVER])									//As long as recover in bucket are under the limit
		{
			buckets[RECOVER]	+= 1;													//Add 1 to the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Reduce Fault-----------------------------------------------------------------
		if (buckets[FAULT] > 0)															//If fault bucket is not empty
		{
			buckets[FAULT]		-= 1;													//Subtract 1 from the fault leaky bucket
		}
		//Reduce Warning---------------------------------------------------------------
		if (buckets[WARNING] > 0)														//If warning bucket is not empty
		{
			buckets[WARNING]	-= 1;													//Subtract 1 from the warning leaky bucket
		}
	}
	else
	{
	#ifdef DEBUG
		UART_Printf("\nUNDER WARNING OVER RECOVER");
	#endif
		//Reduce Recover-------------------------------------------------------
		if (buckets[RECOVER] > 0)														//If recover bucket is not empty
		{
			buckets[RECOVER]	-= 1;													//Subtract 1 from the recover leaky bucket
		}
		//Reduce Permanent Fault-------------------------------------------------------
		if (buckets[PER_FAULT] > 0)														//If permanent fault bucket is not empty
		{
			buckets[PER_FAULT]	-= 1;													//Subtract 1 from the permanent fault leaky bucket
		}
		//Reduce Fault-----------------------------------------------------------------
		if (buckets[FAULT] > 0)															//If fault bucket is not empty
		{
			buckets[FAULT]		-= 1;													//Subtract 1 from the fault leaky bucket
		}
		//Reduce Warning---------------------------------------------------------------
		if (buckets[WARNING] > 0)														//If warning bucket is not empty
		{
			buckets[WARNING]	-= 1;													//Subtract 1 from the warning leaky bucket
		}
	}
}



void common_sytem_status_process(uint16_t *bucket_sizes, uint8_t *buckets, uint16_t mask, uint16_t *per_faults, uint16_t *faults, uint16_t *warnings, uint8_t *clear)
{
	// Toggle system warnings----------------------------------------------------------
	if (buckets[WARNING] >= bucket_sizes[WARNING])										//If warning bucket is full
	{
		*warnings	|= mask;															//Set a system level warning
	}
	// Toggle system faults------------------------------------------------------------
	if (buckets[FAULT] >= bucket_sizes[FAULT])											//If fault bucket is full
	{
		*faults		|= mask;															//Set a system level fault
	}
	// Set system permanent faults-----------------------------------------------------
	if (buckets[PER_FAULT] >= bucket_sizes[PER_FAULT])									//If permanent fault bucket is full
	{
		*per_faults	|= mask;															//Set a system level pFault
	}
	// Recover-------------------------------------------------------------------------
	if (buckets[RECOVER] >= bucket_sizes[RECOVER])									    //If recover bucket is full
	{
		*clear		+=1;																//Clear counter
	}
}



void common_clear_system_faults(uint8_t *clear, uint8_t total, uint16_t mask, uint16_t *faults, uint16_t *warnings)
{
	if (*clear == total)
	{
		*faults		&= ~mask;															//Clear a system level fault
		*warnings	&= ~mask;															//Clear a system level warning
	}
}



uint16_t system_warnings;																// Variable to store secondary fault state
uint16_t system_faults;																	// Variable to store secondary fault state
uint16_t system_per_faults;																// Variable to store permanent fault
uint8_t thermistor_faults;

// 300 bytes
uint8_t under_voltage_bucket    [TOTAL_SECONDARIES][TOTAL_CELLS]      [THRESHOLDS_SIZE];
uint8_t over_voltage_bucket     [TOTAL_SECONDARIES][TOTAL_CELLS]      [THRESHOLDS_SIZE];
uint8_t voltage_delta_bucket	[TOTAL_SECONDARIES]					  [THRESHOLDS_SIZE];
uint8_t under_temperature_bucket[TOTAL_SECONDARIES][TOTAL_THERMISTORS][THRESHOLDS_SIZE];
uint8_t over_temperature_bucket [TOTAL_SECONDARIES][TOTAL_THERMISTORS][THRESHOLDS_SIZE];



void process_secondary_data(int secondary_no)
{
#ifdef DEBUG
    UART_Printf("\nProcess Secondary: %d", secondary_no);
#endif

	uint8_t clear_over = 0;
	uint8_t clear_under = 0;

	int16_t over_thrs [THRESHOLDS_SIZE] = {0};
	int16_t over_time [THRESHOLDS_SIZE] = {0};
	int16_t under_thrs[THRESHOLDS_SIZE] = {0};
	int16_t under_time[THRESHOLDS_SIZE] = {0};
	int16_t delta_thrs[THRESHOLDS_SIZE] = {0};
	int16_t delta_time[THRESHOLDS_SIZE] = {0};

	read_config(READ_CELL_OVER_VOLTAGE,       over_thrs );
	read_config(READ_CELL_OVER_VOLTAGE_TIME,  over_time );
	read_config(READ_CELL_UNDER_VOLTAGE,      under_thrs);
	read_config(READ_CELL_UNDER_VOLTAGE_TIME, under_time);
	read_config(READ_CELL_VOLTAGE_DELTA,      delta_thrs);
	read_config(READ_CELL_VOLTAGE_DELTA_TIME, delta_time);
	
	int16_t delta = 0;
	delta = max_cell_voltage[secondary_no] - min_cell_voltage[secondary_no];
	
#ifdef DEBUG
	UART_Printf("Thresholds: VOLTAGE DELTA:%u < over %u, %u, %u, %u", delta, delta_thrs[RECOVER], delta_thrs[WARNING], delta_thrs[FAULT], delta_thrs[PER_FAULT]);
#endif
	
	common_over_threshold_process(delta, delta_thrs, delta_time, voltage_delta_bucket[secondary_no]);
	common_sytem_status_process(delta_time, voltage_delta_bucket[secondary_no], CELL_VOLTAGE_DELTA_MASK, &system_per_faults, &system_faults, &system_warnings, &clear_over);	

    for (int cell_no = 0; cell_no < TOTAL_CELLS; cell_no++)		// Cycle through all secondary cells     
    {		
		int16_t voltage_reading = bms_ic[secondary_no].cells.c_codes[cell_no] * 0.1 + 2800; //FIXME add offset for testing
		
	#ifdef DEBUG
		UART_Printf("\nProcess cell: %d", cell_no);
		UART_Printf("Thresholds: under %u, %u, %u, %u < CELL_RAW_VOLTAGE:%u < over %u, %u, %u, %u", under_thrs[PER_FAULT], under_thrs[FAULT], under_thrs[WARNING], under_thrs[RECOVER], voltage_reading, over_thrs[RECOVER], over_thrs[WARNING], over_thrs[FAULT], over_thrs[PER_FAULT]);
	#endif	
	
		common_over_threshold_process(voltage_reading, over_thrs, over_time, over_voltage_bucket[secondary_no][cell_no]);
		common_sytem_status_process(over_time, over_voltage_bucket[secondary_no][cell_no], CELL_OVER_VOLTAGE_MASK, &system_per_faults, &system_faults, &system_warnings, &clear_over);
		
		common_under_threshold_process(voltage_reading, under_thrs, under_time, under_voltage_bucket[secondary_no][cell_no]);
		common_sytem_status_process(under_time, under_voltage_bucket[secondary_no][cell_no], CELL_UNDER_VOLTAGE_MASK, &system_per_faults, &system_faults, &system_warnings, &clear_under);

	#ifdef DEBUG	
		UART_Printf("\nAfter Process Cell: %d", cell_no);
		UART_Printf("Over  Voltage: BucketW:%d, BucketF:%d, BucketPF:%d", over_voltage_bucket[secondary_no][cell_no][WARNING], over_voltage_bucket[secondary_no][cell_no][FAULT], over_voltage_bucket[secondary_no][cell_no][PER_FAULT]);
		UART_Printf("Under Voltage: BucketW:%d, BucketF:%d, BucketPF:%d\r\n", under_voltage_bucket[secondary_no][cell_no][WARNING], under_voltage_bucket[secondary_no][cell_no][FAULT], under_voltage_bucket[secondary_no][cell_no][PER_FAULT]);
	#endif
    }	
	
	common_clear_system_faults(&clear_over,  TOTAL_CELLS, CELL_OVER_VOLTAGE_MASK,  &system_faults, &system_warnings);
	common_clear_system_faults(&clear_under, TOTAL_CELLS, CELL_UNDER_VOLTAGE_MASK, &system_faults, &system_warnings);
	
		
	clear_over = 0;
	clear_under = 0;
		
	uint16_t over_mask  = OVER_TEMP_CHARGE_MASK;
	uint16_t under_mask = UNDER_TEMP_CHARGE_MASK;
	
	if (discharge_requested)
	{
		read_config(READ_OVER_TEMPERATURE_CHARGE,       over_thrs );
		read_config(READ_OVER_TEMPERATURE_CHARGE_TIME,  over_time );
		read_config(READ_UNDER_TEMPERATURE_CHARGE,      under_thrs);
		read_config(READ_UNDER_TEMPERATURE_CHARGE_TIME, under_time);
		over_mask  = OVER_TEMP_DISCHARGE_MASK;
		under_mask = UNDER_TEMP_DISCHARGE_MASK;
	}
	else // default to charge limits
	{
		read_config(READ_OVER_TEMPERATURE_DISCHARGE,       over_thrs );
		read_config(READ_OVER_TEMPERATURE_DISCHARGE_TIME,  over_time );
		read_config(READ_UNDER_TEMPERATURE_DISCHARGE,      under_thrs);
		read_config(READ_UNDER_TEMPERATURE_DISCHARGE_TIME, under_time);
	}	
	read_config(READ_PACK_TEMPERATURE_DELTA,      delta_thrs);
	read_config(READ_PACK_TEMPERATURE_DELTA_TIME, delta_time);
	
	thermistor_faults = 0;
	
	for (int therm_no = 0; therm_no < TOTAL_THERMISTORS; therm_no++)	// Cycle through all secondary thermistors
	{
		if ((secondary_therm[secondary_no][therm_no] < (int16_t)MIN_TEMPERATURE) ||
		    (secondary_therm[secondary_no][therm_no] > (int16_t)MAX_TEMPERATURE))  // Check for value out of range
		{
			thermistor_faults++;										// Increase faulty thermistor counter
			UART_Printf("Thermistor %d out of range", therm_no+1);
			continue;
		}
		int16_t temperature_reading = secondary_therm[secondary_no][therm_no] * 0.1;
		
	#ifdef DEBUG
		UART_Printf("Process Thermistor: %d", therm_no+1);
		UART_Printf("Thresholds: under %d, %d, %d, %d < CELL_TEMPERATURE:%d < over %u, %u, %u, %u", (int16_t)under_thrs[PER_FAULT], (int16_t)under_thrs[FAULT], (int16_t)under_thrs[WARNING], (int16_t)under_thrs[RECOVER], (int16_t)temperature_reading, over_thrs[RECOVER], over_thrs[WARNING], over_thrs[FAULT], over_thrs[PER_FAULT]);
	#endif

		common_over_threshold_process(temperature_reading, over_thrs, over_time, under_temperature_bucket[secondary_no][therm_no]);
		common_sytem_status_process(over_time, under_temperature_bucket[secondary_no][therm_no], over_mask, &system_per_faults, &system_faults, &system_warnings, &clear_over);
		common_under_threshold_process(temperature_reading, under_thrs, under_time, under_temperature_bucket[secondary_no][therm_no]);
		common_sytem_status_process(under_time, under_temperature_bucket[secondary_no][therm_no], under_mask, &system_per_faults, &system_faults, &system_warnings, &clear_under);
		
	#ifdef DEBUG
		UART_Printf("\nAfter Process Thermistor: %d", therm_no+1);
		UART_Printf("Over  Temperature: BucketW:%d, BucketF:%d, BucketPF:%d",     over_temperature_bucket [secondary_no][therm_no][WARNING], over_temperature_bucket [secondary_no][therm_no][FAULT], over_temperature_bucket [secondary_no][therm_no][PER_FAULT]);
		UART_Printf("Under Temperature: BucketW:%d, BucketF:%d, BucketPF:%d\r\n", under_temperature_bucket[secondary_no][therm_no][WARNING], under_temperature_bucket[secondary_no][therm_no][FAULT], under_temperature_bucket[secondary_no][therm_no][PER_FAULT]);
	#endif
	}
	
	if (thermistor_faults >= 3)
	{
		system_per_faults |= THERMISTOR_FAULT_MASK;
	}
	
	if (thermistor_faults >= 2)
	{
		system_faults |= THERMISTOR_FAULT_MASK;
	}
	else
	{
		system_faults &= ~THERMISTOR_FAULT_MASK;
	}

	if (thermistor_faults >= 1)
	{
		system_warnings |= THERMISTOR_FAULT_MASK;
	}
	else
	{
		system_warnings &= ~THERMISTOR_FAULT_MASK;
	}
	
	//common_sytem_status_process(thermistor_time, thermistor_bucket, THERMISTOR_FAULT_MASK, &system_per_faults, &system_faults, &system_warnings, &clear_thermistor);
	
	common_clear_system_faults(&clear_over,  TOTAL_THERMISTORS, over_mask,  &system_faults, &system_warnings);
	common_clear_system_faults(&clear_under, TOTAL_THERMISTORS, under_mask, &system_faults, &system_warnings);
	
	UART_Printf("System Warning: %b", system_warnings);
	UART_Printf("System Fault:   %b", system_faults);
	UART_Printf("System P-Fault: %b", system_per_faults);
}
