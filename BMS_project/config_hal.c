/*
 * config_hal.c
 *
 * Created: 10/27/2021 10:14:27 PM
 *  Author: Kostas Stamatakis
 */ 

#include "common.h" 

void i2c_go_to_eeprom_register(uint8_t addr, uint32_t reg)
{
	uint8_t b1, b2, b3;
	b1 = (reg >> 16) & 0x03;
	b2 = (reg >> 8 ) & 0xFF;
	b3 =  reg        & 0xFF;
	I2C_Write( addr + (b1 << 1) );
	I2C_Write(b2);
	I2C_Write(b3);
}

void i2c_write_eeprom(uint8_t* data, uint32_t len)
{
	for (int i=len; i>0; i--)
	{
		//UART_Printf("data[%d]: %x", i-1, data[i-1]);
		I2C_Write(data[i-1]);
	}
}

void i2c_write_word_eeprom(uint16_t value)
{
	WORD word;
	word.w = value;
	
	for (int i=sizeof(uint16_t); i>0; i--)
	{
		//UART_Printf("byte[%d]: %x", i-1, dword.b[i-1]);
		i2c_write_eeprom(&word.b[i-1], sizeof(uint8_t));
	}
	
	return word.w;
}

void i2c_write_dword_eeprom(uint32_t value)
{
	DWORD dword;
	dword.d = value;
	
	for (int i=sizeof(uint32_t); i>0; i--)
	{
		//UART_Printf("byte[%d]: %x", i-1, dword.b[i-1]);
		i2c_write_eeprom(&dword.b[i-1], sizeof(uint8_t));
	}
	
	return dword.d;
}

uint16_t i2c_read_word_eeprom(void)
{
	DWORD dword;
	dword.d = 0;
	
	for (int i=sizeof(uint16_t); i>0; i--)
	{
		dword.b[i-1] = I2C_Read(sizeof(uint8_t));
		//UART_Printf("dword.b[%d]: %x", i-1, dword.b[i-1]);
	}
	
	return dword.d;
}

uint32_t i2c_read_dword_eeprom(void)
{
	DWORD dword;
	dword.d = 0;
	
	for (int i=sizeof(uint32_t); i>0; i--)
	{
		dword.b[i-1] = I2C_Read(sizeof(uint8_t));
		//UART_Printf("dword.b[%d]: %x", i-1, dword.b[i-1]);
	}
	
	return dword.d;
}

int16_t read_single_config(uint8_t command)
{
	int16_t value = 0;
//#ifdef DEBUG
	UART_Printf("read single config");
//#endif
	if (command >= 0x1000)
	{
		command -= 0x1000;
	}
//#ifdef DEBUG
	UART_Printf("address: %X", CONFIG_ADDRESS + (command * sizeof(int16_t)));
//#endif
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, CONFIG_ADDRESS + (command * sizeof(int16_t)));
	I2C_Start();
	I2C_Write(EEPROM_ADDRESS + 1);
	value = i2c_read_word_eeprom();
//#ifdef DEBUG
	UART_Printf("value: %d", value);
//#endif
	return value;
}

void read_config(uint8_t command, int16_t *values)
{
#ifdef DEBUG
	UART_Printf("read config");
#endif
	if (command >= 0x1000)
	{
		command -= 0x1000;
	}
#ifdef DEBUG
	UART_Printf("address: %X", CONFIG_ADDRESS + (command * THRESHOLDS_SIZE * sizeof(int16_t)));
#endif
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, CONFIG_ADDRESS + (command * THRESHOLDS_SIZE * sizeof(int16_t)));
	I2C_Start();
	I2C_Write(EEPROM_ADDRESS + 1);
	for (int i=0; i<THRESHOLDS_SIZE; i++)
	{
		values[i] = i2c_read_word_eeprom();
	#ifdef DEBUG
		UART_Printf("value[%d]: %d", i, values[i]);
	#endif
	}
}

void write_single_config(uint8_t command, int16_t value)
{
//#ifdef DEBUG
	UART_Printf("write single config");
//#endif
	if (command >= 0x1000)
	{
		command -= 0x1000;
	}
//#ifdef DEBUG
	UART_Printf("address: %X", CONFIG_ADDRESS + (command * sizeof(int16_t)));
//#endif
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, CONFIG_ADDRESS + (command * sizeof(int16_t)));
//#ifdef DEBUG
	UART_Printf("value: %d", value);
//#endif
	i2c_write_word_eeprom(value);
	I2C_Stop();
	_delay_ms(7);
}

void write_config(uint8_t command, int16_t *values)
{
#ifdef DEBUG
	UART_Printf("write config");
#endif
	if (command >= 0x1000)
	{
		command -= 0x1000;	
	}
#ifdef DEBUG
	UART_Printf("address: %X", CONFIG_ADDRESS + (command * THRESHOLDS_SIZE * sizeof(int16_t)));
#endif	
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, CONFIG_ADDRESS + (command * THRESHOLDS_SIZE * sizeof(int16_t)));
	for (int i=0; i<THRESHOLDS_SIZE; i++)
	{
	#ifdef DEBUG
		UART_Printf("value[%d]: %d", i, values[i]);
	#endif
		i2c_write_word_eeprom(values[i]);
	}
	I2C_Stop();
	_delay_ms(7);
}

void set_default_config(void)
{
	uint16_t version = 0;
	version = read_single_config(READ_CONFIGURATION_VERSION);
	if (0x5555 == version)
	{
		UART_Printf("Configuration exists - skip defaults");
		return; // exit if configuration has been detected
	}
	UART_Printf("Apply default configurations");
	write_single_config(WRITE_CONFIGURATION_VERSION, 0x5555);
	int16_t values[THRESHOLDS_SIZE];
	values[WARNING]   = 1;
	values[FAULT]     = 2;
	values[PER_FAULT] = 3;
	values[RECOVER]   = 4;
	write_config(WRITE_PACK_SERIAL_NUMBER, values);
	values[WARNING]   = 5;
	values[FAULT]     = 6;
	values[PER_FAULT] = 7;
	values[RECOVER]   = 8;
	write_config(WRITE_BMS_SERIAL_NUMBER, values);
	values[WARNING]   = 9;
	values[FAULT]     = 0;
	values[PER_FAULT] = 1;
	values[RECOVER]   = 2;
	write_config(WRITE_FIRMWARE_REVISION, values);
	values[WARNING]   = 4150;
	values[FAULT]     = 4200;
	values[PER_FAULT] = 4250;
	values[RECOVER]   = 4100;
	write_config(WRITE_CELL_OVER_VOLTAGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_CELL_OVER_VOLTAGE_TIME, values);
	values[WARNING]   = 2500;
	values[FAULT]     = 2000;
	values[PER_FAULT] = 1800;
	values[RECOVER]   = 2600;
	write_config(WRITE_CELL_UNDER_VOLTAGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_CELL_UNDER_VOLTAGE_TIME, values);
	values[WARNING]   = 20;
	values[FAULT]     = 50;
	values[PER_FAULT] = 150;
	values[RECOVER]   = 10;
	write_config(WRITE_CELL_VOLTAGE_DELTA, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_CELL_VOLTAGE_DELTA_TIME, values);
	values[WARNING]   = 55;
	values[FAULT]     = 60;
	values[PER_FAULT] = 65;
	values[RECOVER]   = 50;
	write_config(WRITE_OVER_TEMPERATURE_CHARGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_OVER_TEMPERATURE_CHARGE_TIME, values);
	values[WARNING]   = 2;
	values[FAULT]     = 0;
	values[PER_FAULT] = -10;
	values[RECOVER]   = 5;
	write_config(WRITE_UNDER_TEMPERATURE_CHARGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_UNDER_TEMPERATURE_CHARGE_TIME, values);
	values[WARNING]   = 55;
	values[FAULT]     = 60;
	values[PER_FAULT] = 65;
	values[RECOVER]   = 50;
	write_config(WRITE_OVER_TEMPERATURE_DISCHARGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_OVER_TEMPERATURE_DISCHARGE_TIME, values);
	values[WARNING]   = -15;
	values[FAULT]     = -20;
	values[PER_FAULT] = -25;
	values[RECOVER]   = -10;
	write_config(WRITE_UNDER_TEMPERATURE_DISCHARGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_UNDER_TEMPERATURE_DISCHARGE_TIME, values);
	values[WARNING]   = 10;
	values[FAULT]     = 15;
	values[PER_FAULT] = 20;
	values[RECOVER]   = 5;
	write_config(WRITE_PACK_TEMPERATURE_DELTA, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_PACK_TEMPERATURE_DELTA_TIME, values);
	values[WARNING]   = 80;
	values[FAULT]     = 100;
	values[PER_FAULT] = 120;
	values[RECOVER]   = 50;
	write_config(WRITE_PCB_OVER_TEMPERATURE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_PCB_OVER_TEMPERATURE_TIME, values);
	values[WARNING]   = 33;
	values[FAULT]     = 35;
	values[PER_FAULT] = 40;
	values[RECOVER]   = 30;
	write_config(WRITE_OVER_CURRENT_CHARGE, values);
	values[WARNING]   = 2;
	values[FAULT]     = 3;
	values[PER_FAULT] = 5;
	values[RECOVER]   = 60;
	write_config(WRITE_OVER_CURRENT_CHARGE_TIME, values);
	values[WARNING]   = 30;
	values[FAULT]     = 55;
	values[PER_FAULT] = 55;
	values[RECOVER]   = 25;
	write_config(WRITE_OVER_CURRENT_DISCHARGE, values);
	values[WARNING]   = 10;
	values[FAULT]     = 15;
	values[PER_FAULT] = 20;
	values[RECOVER]   = 60;
	write_config(WRITE_OVER_CURRENT_DISCHARGE_TIME, values);
}

void print_config(void)
{	
	int16_t config[THRESHOLDS_SIZE];
	
	for (int i=0; i<READ_CONFIG_SIZE; i++)
	{
		read_config(READ_PACK_SERIAL_NUMBER + i, config);
		UART_Printf("warning = %d", config[WARNING]);
		UART_Printf("fault   = %d", config[FAULT]);
		UART_Printf("pfault  = %d", config[PER_FAULT]);
		UART_Printf("recover = %d", config[RECOVER]);
	}
}
