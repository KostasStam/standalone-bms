//TODO tidy up this file

#include "common.h"


/************************************* Read me *******************************************
  -The Function wakeup_sleep(TOTAL_SECONDARIES) : is used to wake the LTC681x from sleep state.
   It is defined in LTC681x.cpp
  -The Function wakeup_idle(TOTAL_SECONDARIES) : is used to wake the ICs connected in daisy chain 
   via the LTC6820 by initiating a dummy SPI communication. It is defined in LTC681x.cpp 
*******************************************************************************************/



/********************************************************************************************
 Global Battery Variables received from 681x commands
 These variables store the results from the LTC6813
 register reads and the array lengths must be based
 on the number of ICs on the stack
 *******************************************************************************************/
//FIXME reduce RAM size if possible
cell_asic bms_ic[TOTAL_SECONDARIES];							// Global Battery Variable
int16_t secondary_therm[TOTAL_SECONDARIES][TOTAL_THERMISTORS];	// Array to store secondary temperature readings
uint16_t maxTemperature[TOTAL_SECONDARIES];
uint16_t minTemperature[TOTAL_SECONDARIES];
uint16_t pack_voltage[TOTAL_SECONDARIES];						// Array to store pack Voltage
uint16_t max_cell_voltage[TOTAL_SECONDARIES];					// Array to store max cell voltage
uint16_t min_cell_voltage[TOTAL_SECONDARIES];					// Array to store max cell voltage

/********************************************************************************************
 ADC Command Configurations. See LTC681x.h for options
********************************************************************************************/
#define ADC_CONVERSION_MODE		MD_7KHZ_3KHZ	// ADC Mode
#define ADC_DCP					DCP_DISABLED	// Discharge Not Permitted during ADC conversion
#define AUX_CH_TO_CONVERT		AUX_CH_ALL		// Channel Selection for ADC conversion
#define SEL_ALL_REG				REG_ALL			// Register Selection 
#define SEL_REG_A				REG_1			// Register Selection

/********************************************************************************************
 Initialise LTC
********************************************************************************************/
void ltc_init(void) 
{
	/********************************************************************************************
	 Set configuration register. Refer to the data sheet
	********************************************************************************************/
	bool REFON = true;                                    // Reference Powered Up Bit
	bool ADCOPT = false;                                  // ADC Mode option bit
	// Ensure that Dcto bits are set according to the required discharge time. Refer to the data sheet
	bool FDRF = false;                                    // Force Digital Redundancy Failure Bit
	bool DTMEN = true;                                    // Enable Discharge Timer Monitor
	bool PSBITS[2]= {false,false};                        // Digital Redundancy Path Selection//ps-0,1
	//GPIO2 is used as an output. Write 1 in config registers to set it to High. Write 0 to set it to low.
	//GPIO3 & 6-9 are inputs. Write 1 to GPIO3 & 6-7 to ensure that internal pull down do not affect the analog measurements:
	bool GPIOBITS_B[4] = {true,true,true,true};           // GPIO Pin Control // Gpio 6,7,8,9
	uint16_t UV = UV_THRESHOLD;                           // Under voltage Comparison Voltage
	uint16_t OV = OV_THRESHOLD;                           // Over voltage Comparison Voltage
	bool DCCBITS_A[12] = {false,false,false,false,false,false,false,false,false,false,false,false};   // Discharge cell switch //Dcc 1,2,3,4,5,6,7,8,9,10,11,12
	bool DCCBITS_B[7]= {false,false,false,false,false,false,false};                                   // Discharge cell switch //Dcc 0,13,14,15
	bool DCTOBITS[4] = {true,false,true,false};                                                       // Discharge time value //Dcto 0,1,2,// Programed for 4 min	
	bool GPIOBITS_A[5] = {true,false,true,true,true}; // GPIO Pin Control // Gpio 1,2,3,4,5
	
	quikeval_SPI_connect();
	spi_enable(SPI_CLOCK_DIV16); // This will set the Linduino to have a 1MHz Clock 
	
	LTC6813_init_cfg(TOTAL_SECONDARIES, bms_ic);        // Function to initialize the CFGR data structures (Number of Secondary ICs, Array to store the data) - See LTC6813.h
	LTC6813_init_cfgb(TOTAL_SECONDARIES, bms_ic);       // Function to initialize the CFGR B data structures (Number of Secondary ICs, Array to store the data) - See LTC6813.h  
  
	for (uint8_t secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)					// Loop through all connected secondary LTC devices
	{
		LTC6813_set_cfgr(secondary_no, bms_ic, REFON, ADCOPT, GPIOBITS_A, DCCBITS_A, DCTOBITS, UV, OV);	// Function to set appropriate bits in CFGR register based on bit function - See LTC6813.h
		LTC6813_set_cfgrb(secondary_no, bms_ic, FDRF, DTMEN, PSBITS, GPIOBITS_B, DCCBITS_B);			// Function to set appropriate bits in CFGR B register based on bit function - See LTC6813.h
	}
  
	LTC6813_reset_crc_count(TOTAL_SECONDARIES, bms_ic); // Function that resets the PEC error counters (Number of Secondary ICs, Array to store the data)
	LTC6813_init_reg_limits(TOTAL_SECONDARIES, bms_ic); // Function to initialize register limits (Number of Secondary ICs, Array to store the data)
}



///////////////////////////////////////////////////////////////////////////////////
void find_min_max(uint16_t *array, int length, uint16_t *min, uint16_t *max)
{
	*max = 0;
	*min = 65535;

	for (int i = 0; i < length; i++)
    {
		//UART_Printf("array[i]=%d, max=%d, min=%d", array[i], *max, *min);
		if (array[i] > *max)
		{
			*max = array[i];
			//UART_Printf("array[i] > *max");
		}
		if (array[i] < *min)
		{
			*min = array[i];
			//UART_Printf("array[i] < *min");
		}
    } 
}


/*************************************read_cell_voltages*************************************
  -Function to read Cell ADC channels of all connected LTC secondary devices. Results to be 
   saved in the memory array bms_ic[secondary_no].cells.c_codes[i] for later evaluation. 
  -This LTC function has built in PEC error detection and sample time feedback.
  -See ADC Command Configurations in ModularBMS.h for ADC_CONVERSION_MODE,ADC_DCP,
   CELL_CH_TO_CONVERT definitions.
  -See LTC681x.h and LTC6813.h for full configuration options.
*******************************************************************************************/

void handle_secondary_voltages(int secondary_no)
{
	find_min_max(bms_ic[secondary_no].cells.c_codes, SERIES_CELLS, &min_cell_voltage[secondary_no], &max_cell_voltage[secondary_no]);
	//Stop at cell 6 as remaining cells do not exist

#ifdef DEBUG
	UART_Printf("min cell raw voltage = %d", min_cell_voltage[secondary_no]);
	UART_Printf("max cell raw voltage = %d", max_cell_voltage[secondary_no]);
#endif

	float pack_voltage_print = 0;
	for (int cell_no = 0; cell_no < SERIES_CELLS; cell_no++)
	{
		pack_voltage_print += 0.01 * bms_ic[secondary_no].cells.c_codes[cell_no];
	}
	
	pack_voltage[secondary_no] = (uint16_t)pack_voltage_print;
	
	UART_Printf("PACK_%d VOLTAGE = %d mV", secondary_no, pack_voltage[secondary_no]);

	for (int cell_no = SERIES_CELLS; cell_no < TOTAL_CELLS; cell_no++)  //write 3600mV to all cells above max number of series cells
	{
		bms_ic[secondary_no].cells.c_codes[cell_no] = 36000;
	}
} 


void read_cell_voltages(void)
{
#ifdef DEBUG
  UART_Printf("read cell voltages");
#endif

	int data[4];
	for (int mux_no = 0; mux_no < TOTAL_MUX; mux_no++)                      // Loop through all 4 MUX units
	{
		for (int channel_no=0; channel_no < MAX_CHAN_PER_MUX; channel_no++) // Loop through all 8 channels of the currently selected MUX
		{
			prepare_configuration_data(mux_no, data, channel_no);
			program_ltc();
		}
	}

  uint32_t conv_time = 0;                                       //Setup to log conversion time 
  int8_t error = 0;                                             //Setup for PEC error detection  
  wakeup_sleep(TOTAL_SECONDARIES);                              //Used to wake the LTC681x from sleep state
  
  
  LTC6813_adcv(ADC_CONVERSION_MODE, ADC_DCP, CELL_CH_2and8);	//Start Cell voltage conversion (ADC Mode, Discharge Permitted, Cell Channels to be Measured)
  conv_time = LTC6813_pollAdc();								//Block operation until the ADC has finished it's conversion
  print_conv_time(conv_time);                                   //Print conversion time
 
  // Read Cell Voltage Registers
  wakeup_sleep(TOTAL_SECONDARIES);                              //Used to wake the LTC6813 from sleep state
  error = LTC6813_rdcv(SEL_ALL_REG, TOTAL_SECONDARIES, bms_ic); //Set to read back all cell voltage registers 
  check_error(error);                                           //check error flag and print PEC error message

  //Calculate Total Pack Voltage, Max Cell Voltage, Min Cell Voltage--------------------
  for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)
  {
    handle_secondary_voltages(secondary_no);           
  }
}



/*************************************read_cell_temps****************************************
  - Function to read all connected secondary LTC6813 thermistors connected through GPIO1 via 
    multiple MUX units and GPIO6 - GPIO 9 directly. Results to be processed into 0.1degC
    units and saved in the memory array secondary_therm[][].

    MUX I2C units addressed as follows:
    Therm 0-7    = MUX IC U2 with I2C address 0x4C. Bit shif up by 1 as last bit is 0 (read) = 0x98
    Therm 8-15   = MUX IC U2 with I2C address 0x4F. Bit shif up by 1 as last bit is 0 (read) = 0x9E
    Therm 16-23  = MUX IC U2 with I2C address 0x4D. Bit shif up by 1 as last bit is 0 (read) = 0x9A
    Therm 24-31  = MUX IC U2 with I2C address 0x4E. Bit shif up by 1 as last bit is 0 (read) = 0x9C

    Therm 32-35 = GPIO6 - GPIO9

    Page 35 of  LTC6813-1 Datasheet RevA outlines details of how communications between an 
    LTC secondary device and connected I2C peripherals should be controlled. Steatite have 
    manipulated I2C data bits D1-D4 to cycle through MUX I2C addresses and switches:
    
    https://www.analog.com/media/en/technical-documentation/data-sheets/LTC6813-1.pdf

*******************************************************************************************/

void prepare_configuration_data(int mux_no, int *data, int channel_no)
{
	if (mux_no == 0) data[1] = 0x88;                          // I2C address LSByte combined with Fcom primary NACK(0b1000)
	if (mux_no == 1) data[1] = 0xE8;                          // I2C address LSByte combined with Fcom primary NACK(0b1000)
	if (mux_no == 2) data[1] = 0xA8;                          // I2C address LSByte combined with Fcom primary NACK(0b1000)
	if (mux_no == 3) data[1] = 0xC8;                          // I2C address LSByte combined with Fcom primary NACK(0b1000)

	if (channel_no < TOTAL_CHAN_PER_MUX)                      // Only 8 channels per MUX, after this, turn off MUX channels to stop interference with next MUX
	{
		data[0] = 0x01 << channel_no;							// Bit shift by the switch number for selection of MUX channel
		data[2] = (0xF0 & data[0]) >> 4;						// Select MSB of temporary data byte 'D' and bit shift D2 down by 4 to combine ICOM BLANK with MUX Switch No
		data[3] = (0xF0 & (data[0] << 4) + 0x09);				// Select LSB of temporary data byte 'D' and bit shift up by 4 and combine with Primary NACK +STOP
	}
	else                                                      // Turn off all MUX channels
	{
		data[2] = 0x00;                                         // 0x00 is ICOM Blanc + Switch 0
		data[3] = 0x09;                                         // 0x09 is Channel 0 + ICOM Stop
	}
	
	for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++) //Loop through all connected secondary LTC devices
	{
		// Note I2C address is bit shifted up by 1 for read write bit.
		bms_ic[secondary_no].com.tx_data[0]= 0x69;                // Icom Start(6) + I2C_address D0 (0x4C) + Write bit shift (0x00) [4C -> 98]
		bms_ic[secondary_no].com.tx_data[1]= data[1];             // I2C_address + Fcom primary NACK(8)
		bms_ic[secondary_no].com.tx_data[2]= data[2];             // 0x00 Icom Blank (0) + MUX Switch 1 (0x01)
		bms_ic[secondary_no].com.tx_data[3]= data[3];             // Fcom primary Stop(9)
	}
	
#ifdef DEBUG
	//UART_Printf("D1=%d D2=%d D3=%d", data[1], data[2], data[3]);
#endif
}


void program_ltc(void)
{
	uint32_t conv_time = 0;                                     // Setup to log conversion time
	int8_t error = 0;                                           // Setup for PEC error detection
	
	wakeup_sleep(TOTAL_SECONDARIES);                            // Wake the LTC681x from sleep state
	LTC6813_wrcomm(TOTAL_SECONDARIES, bms_ic);                  // Write to comm register

	wakeup_idle(TOTAL_SECONDARIES);                             // Wake the LTC ICs connected in daisy chain
	LTC6813_stcomm(DATA_LENGTH);                                // Initiates communication between primary and the I2C secondary (Data Length)
	
	wakeup_idle(TOTAL_SECONDARIES);                             // Wake the LTC ICs connected in daisy chain
	error = LTC6813_rdcomm(TOTAL_SECONDARIES, bms_ic);          // Read from comm register
	check_error(error);                                         // Check error flag and print PEC error message

	wakeup_sleep(TOTAL_SECONDARIES);                            // Wake the LTC ICs connected in daisy chain
	LTC6813_adcvax(ADC_CONVERSION_MODE, ADC_DCP);               // Start GPIO1 (MUX Input) conversion (ADC Mode,Balance Discharge Permitted)
	conv_time = LTC6813_pollAdc();								// Block operation until the ADC has finished it's conversion
	
	wakeup_idle(TOTAL_SECONDARIES);                             // Wake the LTC ICs connected in daisy chain
	error = LTC6813_rdaux(SEL_REG_A,TOTAL_SECONDARIES, bms_ic); // Set to read back aux register A
	check_error(error);                                         // Check error flag and print PEC error message
}


float secondary_temperature(int secondary_no)
{
	float voltage = bms_ic[secondary_no].aux.a_codes[0] * 0.0001;	// Read voltage of thermistor
#ifdef DEBUG
	//UART_Printf("bms_ic[secondary_no].aux.a_codes[0]: %d", bms_ic[secondary_no].aux.a_codes[0]);
#endif
	float temp = calculate_temperature(SECONDARY_VOLT, voltage);
#ifdef DEBUG
	UART_Printf("%fV \t %fC", voltage, temp);
#endif
	return temp;
}


void convert_and_save_channel_temperature(int mux_no, int channel_no)
{
	if (channel_no < TOTAL_CHAN_PER_MUX)                        //Save thermistor results to memory array secondary_therm[][]
	{
	#ifdef DEBUG
		UART_Printf("THERMISTOR: %d \t MUX: %d \t CHANNEL: %d", mux_no * 8 + channel_no, mux_no, channel_no);
	#endif
	
		for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)					//Loop through all connected secondary LTC devices
		{
			float temp = secondary_temperature(secondary_no);
			if (isnan(temp)) temp = -3276.7;                                                            // If therm is not connected, type casting will produce a '0' - push result out of range instead.
			secondary_therm[secondary_no][mux_no * TOTAL_CHAN_PER_MUX + channel_no] = (int)(temp * 10); // Save temperature in 0.1C to memory array secondary_therm[][]
		#ifdef DEBUG
			//UART_Printf("secondary_therm[%d][%d]: %d", secondary_no, mux_no * TOTAL_CHAN_PER_MUX + channel_no, secondary_therm[secondary_no][mux_no * TOTAL_CHAN_PER_MUX + channel_no]);
		#endif
		}
	}
}


void handle_mux(int mux_no, int *data)
{
	for (int channel_no=0; channel_no < MAX_CHAN_PER_MUX; channel_no++) // Loop through all 8 channels of the currently selected MUX
	{           
		prepare_configuration_data(mux_no, data, channel_no);
	  
		program_ltc();
		
		convert_and_save_channel_temperature(mux_no, channel_no);
	}
}


void read_cell_temps(void)
{
#ifdef DEBUG
	UART_Printf("\r\nREAD CELL TEMPERATURES:");
#endif

    int data[4];																// Temporary data bytes for storing MUX selection data
	
	for (int mux_no = 0; mux_no < TOTAL_MUX; mux_no++)                          // Loop through all 4 MUX units
	{
		handle_mux(mux_no, data);
	}

	for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)  // Loop through all connected secondary LTC devices
	{	
		//Calculate Max Temperature & Min Temperature--------------------
		find_min_max(secondary_therm[secondary_no], TOTAL_THERMISTORS, &minTemperature[secondary_no], &maxTemperature[secondary_no]);         

		UART_Printf("MIN TEMPERATURE = %f C", minTemperature[secondary_no] * 0.1);
		UART_Printf("MAX TEMPERATURE = %f C", maxTemperature[secondary_no] * 0.1);
	}      
}



/*************************************balance_control****************************************
  - Function to toggle the 'On'/'Off' state of the secondary balance control MOSFETs
  - ADC_DCP variable in ADC Command Configurations in ModularBMS.h configures whether 
    balancing is turned off during ADC voltage measurements. See LTC681x.h and LTC6813.h 
    for full configuration options.

    ---->Need to change line 610 in LTC6813.cpp to stop balancing resistor being applied 
    to all daisy chained secondarys.
*******************************************************************************************/

void balance_control(int secondary_no, int fet, bool state)
{      
  balance_fet[secondary_no][fet] = state;                             //Toggle balance_fet[] array entry 'ON'/'OFF'
  wakeup_sleep(TOTAL_SECONDARIES);                                    //Used to wake the LTC681x from sleep state
  LTC6813_set_discharge(fet, secondary_no + 1, bms_ic);               //Toggle balance 'ON'/'OFF' (SecondaryNumber,2D Array to store data,GPIO bit state)
  LTC6813_wrcfg(TOTAL_SECONDARIES, bms_ic);                           //Write the LTC6813 configuration register A
  LTC6813_wrcfgb(TOTAL_SECONDARIES, bms_ic);                          //Write the LTC6813 configuration register B   
}



/*************************************balance_control****************************************
  - Function to toggle the 'On'/'Off' state of the secondary balance control MOSFETs
  - ADC_DCP variable in ADC Command Configurations in ModularBMS.h configures whether 
    balancing is turned off during ADC voltage measurements. See LTC681x.h and LTC6813.h 
    for full configuration options.

    ---->Need to change line 610 in LTC6813.cpp to stop balancing resistor being applied 
    to all daisy chained secondaries.
*******************************************************************************************/

void balance_all_off(int secondary_no)
{  
  wakeup_sleep(TOTAL_SECONDARIES);                                    //Used to wake the LTC681x from sleep state
  LTC6813_clear_discharge(TOTAL_SECONDARIES,bms_ic);                  //Turn off all balancing
  LTC6813_wrcfg(TOTAL_SECONDARIES,bms_ic);                            //Write the LTC6813 configuration register A
  LTC6813_wrcfgb(TOTAL_SECONDARIES,bms_ic);                           //Write the LTC6813 configuration register B   
  
  for (int cell_no = 0; cell_no < TOTAL_CELLS; cell_no++)
  {
    balance_fet[secondary_no][cell_no] = 0;                           //Turn off balance_fet[] array entry          
  }
}
  


/*************************************read_balance_feedback***********************************
  - Function to read all connected secondary LTC6813 balance feedback GPIO pins and then save
    the balance states into the LTC primary bms_ic[].aux.a_codes[] array.
  - Decode the LTC primary bms_ic[].aux.a_codes[] array to establish whether balancing is 
    taking place, mark the balance_fet[][] I2C/Serial array appropriately.
  - This function should be used after commanding a balance FET on to ensure the balancing
    circuit is functioning correctly.
*******************************************************************************************/

void read_balance_feedback(void)
{
  int8_t   error     = 0;                                                       //Setup for PEC error detection
  uint32_t conv_time = 0;                                                       //Setup to log conversion time

  wakeup_sleep(TOTAL_SECONDARIES);                                              //Used to wake the LTC681x from sleep state
  LTC6813_adax(ADC_CONVERSION_MODE, AUX_CH_TO_CONVERT);                         //Start a GPIO and Vref2 Conversion (ADC conversion mode, GPIO channels to be converted)   
  conv_time = LTC6813_pollAdc();												//Block operation until the ADC has finished it's conversion
  print_conv_time(conv_time);                                                   //Print conversion time

  wakeup_sleep(TOTAL_SECONDARIES);                                              //Used to wake the LTC681x from sleep state
  error = LTC6813_rdaux(SEL_ALL_REG, TOTAL_SECONDARIES, bms_ic);                //Set to read back all aux registers
  check_error(error);                                                           //Check error flag and print PEC error message

  for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)  //Loop through all connected secondary LTC devices
  {  
    if ((bms_ic[secondary_no].aux.a_codes[2] * 0.0001) > 3)                     //(>3V=balancing). Note: GPIO pins <6 = GPIO-1, GPIO pins>=6 = GPIO pin.
    {
      balance_fet[secondary_no][0] = 1;                                         //Cell Balancing
    }
    else
    {
      balance_fet[secondary_no][0] = 0;                                         //Cell Not Balancing
    }            
  } 
}



/*************************************print_conv_time**************************************
  -  LTC Function to print the Conversion Time
  -  Return void
*******************************************************************************************/

void print_conv_time(uint32_t conv_time)
{
  uint16_t m_factor = 1000;  // to print in ms

#ifdef DEBUG
  UART_Printf("(Conversion completed in: %f (ms)", (float)conv_time/m_factor);
#endif
}



/*************************************check_error******************************************
  -  LTC Function to check error flag and print PEC error message
  -  Return void
*******************************************************************************************/
void check_error(int error)
{
  if (error == -1)
  {
    UART_Printf("A PEC error was detected in the received data");
  }
}



/*************************************serial_print_text************************************
  -  LTC Function to print text on serial monitor
  -  Return void
*******************************************************************************************/
void serial_print_text(char data[])
{       
  UART_Printf(data);
}



/******************************************************************************************
 - LTC Brief Hex conversion constants
*******************************************************************************************/

char hex_digits[16] =
{
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};



/******************************************************************************************
 - LTC Brief Global Variables
*******************************************************************************************/

char hex_to_byte_buffer[5] =
{
  '0', 'x', '0', '0', '\0'
};              



/******************************************************************************************
 - LTC Brief Buffer for ASCII hex to byte conversion
*******************************************************************************************/

char byte_to_hex_buffer[3] =
{
  '\0','\0','\0'
};



/*************************************serial_print_hex*************************************
   - LTC Brief Function to print in HEX form
   - Return void
*******************************************************************************************/

void serial_print_hex(uint8_t data)
{
  if (data < 16)
  {
    UART_Printf("0%x", (uint8_t)data);
  }
  else
    UART_Printf("%x", (uint8_t)data);
}
