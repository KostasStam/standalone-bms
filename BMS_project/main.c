#ifdef APPLICATION
/*
 * main.c
 *
 * Created: 2/23/2021 2:07:51 AM
 *  Author: Kostas Stamatakis
 */ 

#include "common.h"


/************************************ Application ID ***************************************/
__attribute__((section(".id"))) const uint8_t volatile application_id[] = { BMS_ID };
	
	
/************************************ Global Variables ***************************************/
bool fan_control[TOTAL_SECONDARIES];					// Variable to store state of GPIO2 output for heater/fan
//bool balance_fet[TOTAL_SECONDARIES][TOTAL_CELLS];		// Array to store balancing FET states

bool charge_requested = 0;
bool discharge_requested = 0;
uint8_t system_state;
uint32_t timer = 0;


void system_initialisations(void) 
{
	//---------------------------------------
	// Map ISR vectors to bootloader section
	//---------------------------------------
	cli();							// disable interrupts
	MCUCR = (1<<IVCE);				// enable interrupt vectors change
	MCUCR = (0<<IVSEL);				// move interrupt vectors to application (necessary after EEPROM backup)
	sei();							// enable interrupts
	//---------------------------------------
		
	wire_init();					// Init MCU timers
	gpio_init();					// Init 500BMS pins
	my_can_init(CAN_BAUDRATE_500K);	// Init CAN port
	uart_init(UART_BAUD_RATE);		// Init UART debug port
	ltc_init();						// Init LTC structures
	i2c_init();						// Init I2C port for EEPROM
	rtc_timer_init();				// Init I2C port for Real Time Clock
	//eeprom_init(); 150 bytes
		
	UART_Printf("\r\n_____________application_____________\r\nID: %X", pgm_read_dword_far(application_id));
		
	//if (backup_firmware()) // returns success of fail // 700 bytes
	{
		//UART_Printf("Firmware backed up successfully");
	}
	//else
	{
		//UART_Printf("Firmware back up failed");
	}
	
	set_default_config();
	print_config();
}


void loop(void)
{
#ifdef DEBUG
	UART_Printf("\r\n==========>  LOOP  <==========");
#endif
	
	for (int i=0; i<TOTAL_SECONDARIES; i++)
	{
		read_and_process_data();		
	}
	
	bool fan_on_conditions = (((system_faults & OVER_TEMP_CHARGE_MASK)    || 
							   (system_faults & OVER_TEMP_DISCHARGE_MASK) || 
							   (system_faults & PCB_OVER_TEMP_MASK))      &&
							  !(system_faults & CELL_UNDER_VOLTAGE_MASK));
			
	if (fan_on_conditions)
	{
		primary_fan_control(ON);
	}
	else
	{
		primary_fan_control(OFF);
	}
	
	/*
	if (system_warnings)
	{
		reduce_current();
	}
	else
	{
		increase_current();
	}
	
	bool charge_voltage = max_cell_voltage[0] < 410;
	
	if (system_faults || system_per_faults)
	{
		fault_state();
	}
	else if (charge_requested && charge_voltage)
	{
		charge_state();
	}
	else if (discharge_requested)
	{
		discharge_state();
	}
	else
	{
		standby_state();
	}*/
	
	static bool led = false;
	if (!(timer % LED_DELAY))
	{
		 if (!led)
		 {
			 led = true;
			 primary_led_control(ON);
		 }
		 else
		 {
			 led = false;
			 primary_led_control(OFF);
		 }
	}
	
	timer++;
}


int main(void)
{
	system_initialisations();
	
	while(1)
	{	
		loop();
    }
}
#endif






/*
primary_fan_control(ON);
contactor_control(1, ON);
rtc_read_time();
test_canbus();
i2c_set_time(23, 59, 59, THURSDAY, 28, 2, 2028); // remember to comment that function after the time is stored and reprogram the application
	
/********** TESTING CAN DEBUGGING FUNCTIONS ************/
/*canbus_debug_int(0x1, 5); //ok
canbus_debug_int(0x2, 1234567890123456789); //ok
canbus_debug_int(0x3, -3); //ok
canbus_debug_int(0x4, -99999999999); //ok
int a = 5;
int16_t b = 4567;
int32_t c = 1283230984;
int64_t d = 1234567890123456789;
int16_t e = -4567;
int32_t f = -1283230984;
int64_t g = -1234567890123456789;
int h = -5;
uint8_t j = 5;
uint16_t k = 4567;
uint32_t l = 3234567890;
uint64_t m = 1234567890123456789;	
canbus_debug_int(0x5, a); //ok
canbus_debug_int(0x6, b); //ok
canbus_debug_int(0x7, c); //??
canbus_debug_int(0x8, d); //ok
canbus_debug_int(0x9, e); //ok
canbus_debug_int(0xa, f); //ok
canbus_debug_int(0xb, g); //ok
canbus_debug_int(0xc, h); //ok
canbus_debug_int(0xd, j); //ok
canbus_debug_int(0xe, k); //ok
canbus_debug_int(0xf, l); //ok
canbus_debug_int(0x10, m); //ok
canbus_debug_float(0x11, 0.7);
canbus_debug_float(0x12, 0.4358908754377);
canbus_debug_float(0x13, -0.7);
canbus_debug_float(0x14, -0.4358908754377);
float n = 0.7;
float o = -0.7;
double p = 0.30495743209579;
double q = -0.503294509;
canbus_debug_float(0x15, n);
canbus_debug_float(0x16, o);
canbus_debug_float(0x17, p);
canbus_debug_float(0x18, q);
canbus_debug("poll");
*/