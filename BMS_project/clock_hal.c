/*
 * clock_hal.c
 *
 * Created: 6/10/2021 5:32:43 PM
 *  Author: Kostas Stamatakis
 */ 

#include "common.h"
	
char* convert_to_day_name(uint8_t day)
{
	switch(day)
	{
		case SUNDAY:
			return "SUNDAY";
		case MONDAY:
			return "MONDAY";
		case TUESDAY:
			return "TUESDAY";
		case WEDNESDAY:
			return "WEDNESDAY";
		case THURSDAY:
			return "THURSDAY";
		case FRIDAY:
			return "FRIDAY";
		case SATURDAY:
			return "SATURDAY";
	}
}

void rtc_timer_init(void)
{
	I2C_Start();
	I2C_Write(0xD0);
	I2C_Write(0x00);
	I2C_Write(0x00);
	I2C_Stop();
}

void rtc_read_time(void)
{
	I2C_Start();
	I2C_Write(0xD0);
	I2C_Write(0x00);
	I2C_Start();
	I2C_Write(0xD1);
	uint8_t seconds = I2C_Read(1);
	uint8_t minutes = I2C_Read(1);
	uint8_t hours   = I2C_Read(1);
	uint8_t day     = I2C_Read(1);
	uint8_t date    = I2C_Read(1);
	uint8_t month   = I2C_Read(1);
	uint16_t year   = I2C_Read(1);
	seconds = ((seconds & 0x70)>>4) * 10 + (seconds & 0x0F);
	minutes = ((minutes & 0x70)>>4) * 10 + (minutes & 0x0F);
	hours   = ((hours   & 0x30)>>4) * 10 + (hours   & 0x0F);
	day    &= 0x07;
	date    = ((date    & 0x30)>>4) * 10 + (date   & 0x0F);
	month   = ((month   & 0x10)>>4) * 10 + (month  & 0x0F);
	year    = ((year    & 0xF0)>>4) * 10 + (year   & 0x0F) + 2020;
	UART_Printf("%d:%d:%d", hours, minutes, seconds);
	UART_Printf("%s %d/%d/%d", convert_to_day_name(day), date, month, year);
}

void i2c_set_time(uint8_t hours, uint8_t minutes, uint8_t seconds, uint8_t day, uint8_t date, uint8_t month, uint16_t year)
{
	UART_Printf("setting clock to:");
	UART_Printf("%d:%d:%d", hours, minutes, seconds);
	UART_Printf("%s %d/%d/%d", convert_to_day_name(day), date, month, year);
	I2C_Start();
	I2C_Write(0xD0);
	I2C_Write(0x00);
	seconds = ((seconds/10)<<4) + seconds - (seconds/10)*10;
	minutes = ((minutes/10)<<4) + minutes - (minutes/10)*10;
	hours   = ((hours  /10)<<4) + hours   - (hours  /10)*10;
	date    = ((date   /10)<<4) + date    - (date   /10)*10;
	month   = ((month  /10)<<4) + month   - (month  /10)*10;
	year   %= 2020;
	year    = ((year   /10)<<4) + year    - (year   /10)*10;
	I2C_Write(seconds);
	I2C_Write(minutes);
	I2C_Write(hours);
	I2C_Write(day);
	I2C_Write(date);
	I2C_Write(month);
	I2C_Write(year);
	I2C_Write(0x00);
	I2C_Stop();
}