#include "common.h"

/*************************************balance*****************************************
 -This function manages the balancing of all cells within the battery system. 
 Each secondary module is cycled through in turn, each cell within that module is then 
 compared to the preset balance threshold in the user configurable EEPROM. 
 A hysteresis system is used to prevent rapid oscillation of the balancing MOSFETs 
***************************************************************************************/

void handle_cell_balancing(int cell_no, int secondary_no)
{
#ifdef DEBUG
    UART_Printf("Cell: %d : %d : %d", cell_no + 1, bms_ic[secondary_no].cells.c_codes[cell_no], balance_fet[secondary_no][cell_no]); 
#endif

	//uint32_t balance_off_volt;

    bool is_secondary_balancing         = balance_fet[secondary_no][cell_no];
    bool is_cell_volt_below_balance_off = bms_ic[secondary_no].cells.c_codes[cell_no] <= balance_off_volt;
    
    if (is_secondary_balancing && is_cell_volt_below_balance_off)                           //If already balancing and cell voltage below balance recovery voltage
    {
        balance_control(secondary_no, cell_no, OFF);                                        //Switch balancing off

        #ifdef DEBUG
            UART_Printf("Balancing Off: %d\n", balance_fet[secondary_no][cell_no]);
        #endif
    }
    else if (!is_cell_volt_below_balance_off)                                               //Else not already balancing and cell voltage above balance threshold
    {
        balance_control(secondary_no, cell_no, ON);                                         //Switch balancing on

        #ifdef DEBUG
            UART_Printf("Balancing On: %d\n", balance_fet[secondary_no][cell_no]);
        #endif     
    }
}


#define PCB_THERM 9 //Note: Because 36 is not working

void handle_secondary_boards(int secondary_no)
{
    //Hysteresis is formed by bitwise and between present PCB temperature fault and hysteresis value
    bool warning_state      = ((secondary_warnings[secondary_no] & OVER_TEMP_MASK) / OVER_TEMP_MASK);
    int  applied_hysterisis = (warning_state * config.balance_temp_hyst);
    int  temp_threshold     = (config.balance_temp_thresh - applied_hysterisis);

    //Check secondary PCB isn't too hot for balancing
    if (secondary_therm[secondary_no][PCB_THERM] < temp_threshold)
    {
        secondary_warnings[secondary_no] = (secondary_warnings[secondary_no] & !OVER_TEMP_MASK);  //Clear Secondary over temperature PCB Warning

        for (int cell_no = 0; cell_no < TOTAL_CELLS; cell_no++)									//Cycle through all cells in secondary module 
        {
            handle_cell_balancing(cell_no, secondary_no);
        }
    }
    else                                                                                        //Secondary PCB too hot for balancing
    {        
        balance_all_off(secondary_no);                                                          //Switch off all balancing on hot secondary PCB
        secondary_warnings[secondary_no] = (secondary_warnings[secondary_no] | OVER_TEMP_MASK);   //Flag secondary over temperature PCB warning
    }
}



void balance(void)
{
#ifdef DEBUG    
    UART_Printf("Balancing:\n");
#endif

    for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)				//Cycle through all secondary boards
    {
    #ifdef DEBUG
        UART_Printf("Secondary Temp: %d\n", secondary_therm[secondary_no][9]);
    #endif

        handle_secondary_boards(secondary_no);
    }
}
