/*
 * logging_hal.c
 *
 * Created: 10/27/2021 10:23:16 PM
 *  Author: Kostas Stamatakis
 */ 

typedef struct {
	char date_time_stamp[30];
	char variable[20];
	} Logger;
	

void log_var(char *c, char *dts) {
	
	UART_Printf(">>log %s, %s", c, dts);
	
	Logger logger;
	logger.variable = c;
	logger.date_time_stamp = dts;
	static uint8_t addr_offset = 0;
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, LOGS_ADDRESS);
}