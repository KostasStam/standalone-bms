/****************************************************************************************
* Project: Steatite Modular BMS System
* Developed by: Matthew Adams
* Filename: ModularBMS
* Created on: 18/03/2020
* Modified on: 23/07/2020
* Version: 0.1.1
* 
*
* NOTES
* Setup:
* Set the serial terminal baud rate to 115200 and select the newline terminator.
*   
* https://www.steatite-batteries.co.uk/
*
******************************************************************************************
* Copyright 2020 Steatite Ltd.
* All rights reserved.
*/


#include "common.h"



/************************************* Main Program Loop************************************
  -Main loop for reading data from secondary devices, acting upon changes in measured data,
   handling communications through serial/CAN/RS485(Modbus) and updating long term 
   EEPROM variable storage.
*******************************************************************************************/

void print_info(void)
{
#ifdef ENABLE_I2C
  //Read Voltage from CMS over I2C
  uint8_t c = 0;
  uint8_t d = 0;
  myWire.beginTransmission(9); 
  myWire.write((uint8_t)0);//page
  myWire.write((uint8_t)26);//line
  myWire.endTransmission();
  myWire.requestFrom(9, 2);
  c = myWire.read();
  d = myWire.read();
  int pack_voltage = (int)d << 8; 
  pack_voltage = pack_voltage + c;
  UART_Printf("Pack Voltage------:");
  UART_Printf(pack_voltage);

  //Read SOC from CMS over I2C    
  myWire.beginTransmission(9); 
  myWire.write((uint8_t)0);//page
  myWire.write((uint8_t)30);//line
  myWire.endTransmission();
  myWire.requestFrom(9, 1);
  BQSOC[0] = myWire.read();  
  UART_Printf("BQSOC------:");
  UART_Printf(BQSOC[0]);

  //Read Cycle Count from CMS over I2C    
  myWire.beginTransmission(9); 
  myWire.write((uint8_t)0);//page
  myWire.write((uint8_t)32);//line
  myWire.endTransmission();
  myWire.requestFrom(9, 1);
  BQCycleCount[0] = myWire.read();  
  UART_Printf("BQCycleCount------: %d\n", BQCycleCount[0]);

  //Display Cell Voltages*********************
  UART_Printf("\n");
  UART_Printf(F("Cell Voltages:"));
  for (int secondary_no = 0 ; secondary_no < TOTAL_SECONDARIES; secondary_no++)
  {
    UART_Printf("IC ");
    UART_Printf(secondary_no + 1, DEC);
    UART_Printf("\n");
    for (int i=0; i<bms_ic[0].ic_reg.cell_channels; i++)
    {
      UART_Printf(" C");
      UART_Printf(i + 1, DEC);
      UART_Printf(":");
      UART_Printf(bms_ic[secondary_no].cells.c_codes[i] * 0.0001, 4);
      UART_Printf("\n");
    }
    UART_Printf("");
  }

  UART_Printf("");
#endif
}






#ifdef NOT_USED
  //Display Cell Voltages*********************
      UART_Printf("\n");
      UART_Printf(F("Cell Voltages:"));
      for (int secondary_no = 0 ; secondary_no < TOTAL_SECONDARIES; secondary_no++)
      {
        UART_Printf("IC ");
        UART_Printf(secondary_no+1,DEC);
        UART_Printf("\n");
        for (int i=0; i<bms_ic[0].ic_reg.cell_channels; i++)
          {
          UART_Printf(" C");
          UART_Printf(i+1,DEC);
          UART_Printf(":");
          UART_Printf(bms_ic[secondary_no].cells.c_codes[i]*0.0001,4);
          UART_Printf("\n");
          }
        UART_Printf("");
      }


      //Display Cell Temp----------------------------------------------------------------
      UART_Printf(F("Cell Temperatures:"));
      for (int secondary_no =0 ; secondary_no < TOTAL_SECONDARIES; secondary_no++)
      {
         UART_Printf(" IC ");
         UART_Printf(secondary_no+1,DEC);
          
        for (int i = 0; i<36; i++)
          {    
               UART_Printf(F(" Therm"));
               UART_Printf(i,DEC);
               UART_Printf(": ");
               UART_Printf(secondary_therm[secondary_no][i]);
               UART_Printf("\n");
          }
          UART_Printf("");
      }


      //Display Balance Feedback----------------------------------------------------------
      UART_Printf("GPIO3 Balancing Feedback (1=Balancing, 0=Not Balancing):    ");
      UART_Printf(balance_fet[0][0]);
      UART_Printf("");
      
      //Display Primary Temperature-------------------------------------------------------
      UART_Printf(F("Primary PCB Temperature: "));
      UART_Printf(F(" Degrees Celsius"));
      UART_Printf("");


    //Display Balance Array--------------------------------------------------------------
  
    UART_Printf(F("Balance Array"));
    for (uint8_t secondary_no = 0; secondary_no<TOTAL_SECONDARIES;secondary_no++)
        {
        for (int j=0; j<19; j++)
            {
            UART_Printf("Balance FET");
            UART_Printf(j);
            UART_Printf(": ");
            UART_Printf(balance_fet[secondary_no][j]);
            UART_Printf("");   
            }
        UART_Printf("");   
        }


    //Display EEPROM Values------------------------------------------------------------
      UART_Printf("EEPROM Values:");
    for (int i=0; i<50; i++)
      {
      UART_Printf(eeprom[i]);
      }

UART_Printf("\n");
#endif
