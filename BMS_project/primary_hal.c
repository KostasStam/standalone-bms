#ifdef APPLICATION

#include "common.h"

/******************************* low level control functions ********************************/

/*
// GPIO --------------------------------------------
void setBit (volatile uint8_t * reg, uint8_t pin) 
{
	*reg |= (1 << pin);
}

void resetBit (volatile uint8_t * reg, uint8_t pin) 
{
	*reg &= !(1 << pin);
}
*/

// Common
float calculate_temperature(float volts, float voltage)
{
	float therm_res = (voltage * RESISTANCE) / (volts - voltage);	// Calculate Resistance of therm
	float logarithm = log( therm_res / ( RESISTANCE * exp(-14.0533) ) );
	float temp = (4190 / logarithm) - 273.15 ;						// Calculate temperature
#ifdef UNITS
	temp = (273.15 + temp) * 10;									// Change units to 0.1Kelvin
#endif
	return temp;
}


void gpio_init(void)
{
	GPIO_PinDirection(PRIMARY_LED_PIN,	OUTPUT);
	GPIO_PinDirection(SWITCH_5V_PIN,	OUTPUT);
	GPIO_PinDirection(FAN_PIN,			OUTPUT);
	GPIO_PinDirection(CONTACTOR_1_PIN,	OUTPUT);
}


/*************************************primary_led_control***********************************
  -Input to function is a boolean on/off value used to control the Primary LED
  -This is a simple function used to both control the status of the primary LED directly,
   and flag the primary_led memory array as on/off, enabling the main loop or host device to 
   know the current status of the LED.
*******************************************************************************************/

bool primary_led = 0;							// Variable to store the state of the Primary PCB LED

void primary_led_control(bool control)
{
  if (ON == control)
  {
      primary_led = ON;							//Flag as On in the 'primary_led' variable
	  GPIO_PinWrite(PRIMARY_LED_PIN, ON);		//Turn the LED on (HIGH is the voltage level)  
  }
  else
  {
      primary_led = OFF;						//Flag as Off in the 'primary_led' variable
	  GPIO_PinWrite(PRIMARY_LED_PIN, OFF);		//Turn the LED on (LOW is the voltage level)
  }
}



/*************************************switch_5v_control**************************************
  -Input to function is a boolean on/off value used to control the Primary 5v switch
  -This is a simple function used to both control the status of the primary 5v directly
*******************************************************************************************/

bool switch_5v = 0;								// Variable to store the state of the Primary PCB 5V switch

void primary_switch_5v_control(bool control)
{
  if (ON == control)
  {
      switch_5v = ON;							//Flag as On in the 'switch_5v' variable
	  GPIO_PinWrite(SWITCH_5V_PIN, ON);			//Turn the 5V on (HIGH is the voltage level)  
  }
  else
  {
      switch_5v = OFF;							//Flag as Off in the 'switch_5v' variable
	  GPIO_PinWrite(SWITCH_5V_PIN, OFF);		//Turn the 5V on (LOW is the voltage level)
  }
}



/*************************************fan_control**************************************
  -Input to function is a boolean on/off value used to control the Primary 5v switch
  -This is a simple function used to both control the status of the primary 5v directly
*******************************************************************************************/

bool fan = 0;									// Variable to store the state of the Primary PCB 5V switch

void primary_fan_control(bool control)
{
  if (ON == control)
  {
      fan = ON;									//Flag as On in the 'fan' variable
	  GPIO_PinWrite(FAN_PIN, ON);				//Turn the fan on (HIGH is the voltage level)  
  }
  else
  {
      fan = OFF;								//Flag as Off in the 'fan' variable
	  GPIO_PinWrite(FAN_PIN, OFF);				//Turn the fan on (LOW is the voltage level)
  }
}



/*************************************contactor_control**************************************
  -Input to function is a boolean on/off value used to control the contactor.
  -This is a simple function used to both control the contactor directly,and flag the 
   contactor memory array as on/off, enabling the main loop or host device to 
   know the current status of the contactor.
*******************************************************************************************/

bool contactor[TOTAL_CONTACTORS];						// Variable to store the state of the Primary PCB Contactors

void contactor_control(int contactor_no, bool control)	//Changed to bool from int - not tested!
{
  int contactor_pin = CONTACTOR_1_PIN;					//Default contactor 1

  switch(contactor_no)
  {
    case 1:
      contactor_pin = CONTACTOR_1_PIN;
      break;
  }

  if (ON == control)
  {
      contactor[contactor_no] = ON;						//Flag as on in the 'contactor[]' array
	  GPIO_PinWrite(contactor_pin, ON);					// turn contactor 'on' (HIGH is the voltage level)
  }
  else
  {
      contactor[contactor_no] = OFF;                    //Flag as off in the 'contactor[]' array
	  GPIO_PinWrite(contactor_pin, OFF);				// turn contactor 'off' (LOW is the voltage level)
  }
}



/*************************************contactor_control**************************************
  -Function to switch all contactors off. Calls contactor_1_control, contactor_2_control and 
   contactor_3_control with an input of 0 (OFF).
*******************************************************************************************/
void contactors_off()
{
  contactor_control(1, OFF);							//Turn off contactor1
}
 


/*************************************read_primary_temp****************************************
  -Simple function to read the ADC channel on the primary micro controller where the Primary
   on board thermistor is connected. The raw ADC value is converted to units of 
   0.1 degrees Celsius and stored in the 'primary_temp' variable. 
*******************************************************************************************/
#if 0 // not used for BMS500
int primary_temp = 0;									// variable to store the value coming from the sensor

void read_primary_temp()
{	
	uint16_t voltage = ADC_GetAdcValue(PRIMARY_THERM_CHL);
	primary_temp = calculate_temperature(PRIMARY_VOLT, voltage);

#ifdef UNITS
	primary_temp = (273.15 + primary_temp) * 10;		//change units to 0.1K
#endif

#ifdef DEBUG
	UART_Printf("Primary Thermistor Voltage / Temperature: %d \t %d", voltage, primary_temp);
#endif
}
#endif
#endif