/*
 * backup.c
 *
 * Created: 5/28/2021 12:22:48 PM
 *  Author: Kostas Stamatakis
 */ 

#include "common.h"
#include "Bootloader/flash.h"
#include "Bootloader/nvm.h"


bool check_for_eeprom_firmware(void)
{
	UART_Printf("Check for firmware");
    #define CHECKSUM_ADDRESS	(0x19FF4)
	#define FILE_LENGTH_ADDRESS	(0x19FF8)
    uint32_t signature_checksum = 0;
	uint32_t file_length = 0;
	
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, (uint32_t)FlashGetUserProgBaseAddress());
	I2C_Write(EEPROM_ADDRESS + 1);
    
    /* verify the checksum based on how it was written by FlashWriteChecksum(). */
    for (uint32_t i=0; i<FLASH_WRITE_BLOCK_SIZE; i+=sizeof(uint32_t))
    {
        signature_checksum += i2c_read_dword_eeprom();
    }
	
	I2C_Stop();
	_delay_ms(7);
	
    /* add the checksum value that was written by FlashWriteChecksum(). x1ESince this was a
    * Two complement's value, the resulting value should equal 0.      x20
    */
	signature_checksum += pgm_read_dword_far(CHECKSUM_ADDRESS);
	//UART_Printf("%X", pgm_read_dword_far(CHECKSUM_ADDRESS));
	
    /* sum should add up to an unsigned 32-bit value of 0 */
    if (signature_checksum != 0)
    {
        /* checksum not okay */
        UART_Printf("Error Checksum");
		return false;
    }
    else
    {
        /* checksum okay */
        UART_Printf("Checksum Verified");
    }
	
	file_length = pgm_read_dword_far(FILE_LENGTH_ADDRESS);
	//UART_Printf("File Length = %X", file_length);
	
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, file_length - sizeof(uint32_t));
	I2C_Start();
	I2C_Write(EEPROM_ADDRESS + 1);
	
	if (0xFFFFFFFF == i2c_read_dword_eeprom())
	{
		// no firmware
		I2C_Stop();
		UART_Printf("No firmware detected in EEPROM");
		return false;
	}
	else
	{
		// firmware found
		UART_Printf("Firmware detected in EEPROM");
	}
	
	if (0xFFFFFFFF != i2c_read_dword_eeprom())
	{
		// no firmware
		I2C_Stop();
		UART_Printf("No blank area detected right after firmware in EEPROM");
		return false;
	}
	else
	{
		// firmware found
		I2C_Stop();
		UART_Printf("Blank area detected right after firmware in EEPROM");
		return true;
	}
}



void erase_eeprom_firmware(void)
{
	UART_Printf("Erase EEPROM");
	
	uint8_t  f[]             = { 0xFF, 0xFF, 0xFF, 0xFF };
	uint32_t eeprom_register = (uint32_t)FlashGetUserProgBaseAddress();

	I2C_Start();
	
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, eeprom_register);
		
	for (uint32_t addr=FlashGetUserProgBaseAddress(); addr<(uint32_t)CONFIG_ADDRESS; addr+=sizeof(uint32_t))
	{				
		i2c_write_eeprom((uint8_t*)&f, sizeof(f));
		//UART_Printf("Erase EEPROM %X < %X", i, CONFIGURATION_ADDRESS);
						
		eeprom_register += sizeof(uint32_t);
			
		if (0 == (eeprom_register % EEPROM_PAGE_SIZE))
		{
			I2C_Stop();
			_delay_ms(7);
			I2C_Start();
			i2c_go_to_eeprom_register(EEPROM_ADDRESS, eeprom_register);
			UART_TxChar('.');
		}
			
	}
		
	I2C_Stop();
		
	_delay_ms(7);
	
	UART_Printf("EEPROM Firmware Erased");
}



bool backup_firmware(void)
{
	bool result = true;
	uint32_t	eeprom_register = (uint32_t)FlashGetUserProgBaseAddress();	
	uint32_t	eeprom_data;
	DWORD		flash_data;
	flash_data.d = 0;	
	
	if (check_for_eeprom_firmware())
	{		
		UART_Printf("Skip Firmware Backup");
		_delay_ms(7);
		return true;
	}
	
	// Create Backup
	
	erase_eeprom_firmware();
	
	UART_Printf("Start Backup");
	
	I2C_Start();
	
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, eeprom_register);
		
	for (uint32_t addr=FlashGetUserProgBaseAddress(); addr<FlashGetEndAddress(); addr+=sizeof(uint32_t))
	{
		flash_data.d = pgm_read_dword_near(addr);
		
		//UART_Printf("flash_data[%X]=%X", addr, flash_data.d);
		
		if (0xFFFFFFFF == flash_data.d)
		{
			UART_Printf("Blank flash area detected at %X", addr);
			flash_data.d = pgm_read_dword_far(APPLIC_ID_ADDR);
			I2C_Stop();
			_delay_ms(7);			
			UART_Printf("Finalise copying. Write ID %X", addr, flash_data.d);
			I2C_Start();
			i2c_go_to_eeprom_register(EEPROM_ADDRESS, APPLIC_ID_ADDR);
			i2c_write_eeprom((uint8_t*)&flash_data.b, sizeof(flash_data.d));
			break;
		}
		
		i2c_write_eeprom((uint8_t*)&flash_data.b, sizeof(flash_data.d));
		
		eeprom_register += sizeof(uint32_t);
		
		if ((0 == (eeprom_register % EEPROM_PAGE_SIZE)) && eeprom_register < (FlashGetEndAddress() - FlashGetUserProgBaseAddress() - FLASH_SECTOR_SIZE))
		{
			I2C_Stop();
			_delay_ms(7);
			I2C_Start();
			i2c_go_to_eeprom_register(EEPROM_ADDRESS, eeprom_register);
		}
		
	}
	
	I2C_Stop();
	
	_delay_ms(7);
	
	
	// Verify Backup
	
	UART_Printf("Verify Backup");
	
	I2C_Start();
	
	eeprom_register = FlashGetUserProgBaseAddress();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, eeprom_register);
	
	I2C_Start();
	I2C_Write(EEPROM_ADDRESS + 1);
	
	for (uint32_t addr=FlashGetUserProgBaseAddress();  addr<FlashGetEndAddress();  addr+=sizeof(uint32_t))
	{
		flash_data.d = pgm_read_dword_near(addr);
		
		if (0xFFFFFFFF == flash_data.d)
		{
			UART_Printf("Blank flash area detected at %X. Stop checking", addr);
			break;
		}
		
		eeprom_data = i2c_read_dword_eeprom();
		
		if (flash_data.d != eeprom_data)
		{
			UART_Printf("addr %X, \tflash data  %X", addr, flash_data.d);
			UART_Printf("reg  %X, \teeprom data %X", eeprom_register, eeprom_data);
			result = false;
			break;
		}
		
		eeprom_register += sizeof(uint32_t);
	}
	
	I2C_Stop();
	
	return result;
}



void prepare_eeprom(uint8_t addr, uint32_t reg)
{
	UART_Printf("\r\nStart Recovery");
	
	I2C_Start();
	
	i2c_go_to_eeprom_register(addr, reg);
	
	I2C_Start();
	I2C_Write(addr + 1);
}



bool handle_last_data(uint32_t addr, uint8_t data_index, blt_int8u* eeprom_data)
{
	uint32_t bytes = data_index * sizeof(uint32_t);
	
	// program the data
	if (NvmWrite(addr - bytes, bytes, (blt_int8u*)eeprom_data) == BLT_FALSE)
	{
		UART_Printf("Recovery Failed");
		return false;
	}
	
	// call erase/programming cleanup routine 
	if (NvmDone() == BLT_FALSE)
	{
		UART_Printf("error xcp generic nvmdone");
		return false;
	}
	
	return true;
}



uint32_t detect_firmware_end_addr(uint32_t start_addr)
{	
	I2C_Start();
	i2c_go_to_eeprom_register(EEPROM_ADDRESS, start_addr);
	I2C_Start();
	I2C_Write(EEPROM_ADDRESS + 1);
	
	for (uint32_t addr=start_addr;  addr<FlashGetEndAddress();  addr+=sizeof(uint32_t))
	{		
		uint32_t eeprom_dword = i2c_read_dword_eeprom();
		
		//UART_Printf("detect eeprom_dword[%X]=%X", addr, eeprom_dword);
		
		// check for no data
		if (0xFFFFFFFF == eeprom_dword)
		{
			return addr;
		}
	}
	
	
	
	return 0;
}


// Recover Firmware Backup from EEPROM
bool recover_firmware(void)
{

	bool		result				= true;
	uint8_t		data_index			= 0;
	uint8_t		length_in_dwords	= LENGTH_DWORDS;
	uint32_t	length_in_bytes		= LENGTH_BYTES;
	uint32_t	start_addr			= FlashGetUserProgBaseAddress();
	uint32_t	end_addr;		
	uint32_t	firmware_size		= detect_firmware_end_addr(start_addr) - FlashGetUserProgBaseAddress();
	uint32_t	eeprom_data[ LENGTH_DWORDS ];
	
	UART_Printf("firmware_size=%X", firmware_size);
				
	// erase the memory
	if (NvmErase(start_addr, firmware_size) == BLT_FALSE)
	{
		UART_Printf("Error Firmware Erase Failed");
		return false;
	}
	if (NvmErase(APPLIC_ID_ADDR, sizeof(uint32_t)) == BLT_FALSE)
	{
		UART_Printf("Error Application ID Erase Failed");
		return false;
	}
	
	prepare_eeprom(EEPROM_ADDRESS, APPLIC_ID_ADDR);
		
	uint32_t id_data = i2c_read_dword_eeprom();
	I2C_Stop();	
	_delay_ms(7);
		
	blt_int8u id[4];
	for (int i=0; i<4; i++)
	{
		id[i] = (id_data >> i*8) & 0xFF;
	}
		
	// program the application ID
	if (NvmWrite(APPLIC_ID_ADDR, sizeof(uint32_t), id) == BLT_FALSE)
	{
		UART_Printf("Error Programming Application ID");
		return false;
	}

	prepare_eeprom(EEPROM_ADDRESS, start_addr);
	
	for (uint32_t addr=FlashGetUserProgBaseAddress();  addr<FlashGetEndAddress();  addr+=sizeof(uint32_t))
	{		
		eeprom_data[data_index] = i2c_read_dword_eeprom();
		
		//UART_Printf("eeprom_data[%d]=%X", data_index, eeprom_data[data_index]);
		
		// check for no data
		if (0xFFFFFFFF == eeprom_data[data_index])
		{
			result = handle_last_data(addr, data_index, (uint8_t*)eeprom_data);
			
			UART_Printf("Handle last data");
			
			break;
		}
		
		// check for new page
		if (data_index >= length_in_dwords - 1)
		{			
			data_index = 0;
			
			// program the data
			if (NvmWrite(addr - length_in_bytes + sizeof(uint32_t), length_in_bytes, (uint8_t*)eeprom_data) == BLT_FALSE)
			{
				UART_Printf("Error Programming Firmware");
				result = false;
				break;
			}
		}
		else
		{
			data_index++;			
		}		
	}
	
	return result;
}
