#include "common.h"

/*************************************receiveEvent*****************************************
  -Interupt event function that triggers whenever data is received from the primary I2C 
   device. 
  -A minimum of 2 bytes are required; page number and line number. If only 2 bytes 
   are received the BMS will treat this as a forthcoming request for data, it will prepare 
   for this by moving the relevant data (page/line location) into a 2 byte array buffer 
   'I2C_Array_Tx[]'.
  -If there are further bytes received, these are data bytes and will be used to re-program
   the requested memory location (page/line) if permitted.   
*******************************************************************************************/

void flash_led(void)
{
    if (digitalRead(PRIMARY_LED_PIN))
    {
        digitalWrite(PRIMARY_LED_PIN, LOW);
    }
    else
    {            
        digitalWrite(PRIMARY_LED_PIN, HIGH);
    }
}


#ifdef ENABLE_I2C
void receiveEvent(int i2c_num_bytes) 
{
    UART_Printf("\nReceive Event I2C !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"); 
 
    int page = Wire.read();                 //Variable to store incoming page number
    int line = Wire.read();                 //Variable to store incoming line number

    if (i2c_num_bytes > 2)                  //If more than 2 bytes received over I2C, the next bytes are data       
    {
        int data_lsb = Wire.read();         //Receive LSB first  
        int data_msb = 0;                   //Ensure default MSB is zero incase only 1 data byte received
        data_msb = Wire.read();             //Receive MSB
        int data = (int)data_msb << 8;      //Bit shift MSB up by 8 and store in 'data'
        data = data + data_lsb;             //No need to bit shift LSB, add to 'data'                          
        //write_registers(page, line, data);  //Write 'data' received to BMS SRAM register
    }                                     
    else                                    //If only 2 bytes received over I2C, it is a read request only.
    {
        //read_registers(page, line);         //Read specified BMS SRAM register into I2C_Array_Tx[] buffer
    } 

    flash_led();                            //Flash Primary LED to show I2C comms are live - remove after BMS development       
}
#endif


/*************************************requestEvent*****************************************
  -Interupt event function that triggers whenever a read request is detected from the 
   primary over I2C.
  -This event should be preceded by a receive event from the primary I2C, defining the
   location of the data that is now to be sent. The primary BMS should now have the requested
   data already stored in the 'I2C_Array_Tx[]' array in single byte format, for a rapid 
   response to the primarys request.
*******************************************************************************************/

#ifdef ENABLE_I2C
void requestEvent() 
{
    UART_Printf("\nRequest Event I2C ******************************************\n"); 

    Wire.write(I2C_Array_Tx[0]);            //Write LSB of data to the primary I2C device
    Wire.write(I2C_Array_Tx[1]);            //Write MSB of data to the primary I2C device

}
#endif


/*************************************read_registers*****************************************
  -Inputs to function are memory array Page and Line values used to locate the requested 
   data to be stored in the I2C buffer for later transmission on the I2C bus, or direct 
   printing to the serial/USB port.
  -This section uses a decode (multiple if/else statements) to find the relevant array
   storage area for the requested data. Different data requests need to be processed 
   differently depending upon the type of data stored in that area, before the data is
   either stored in the I2C_Array_Tx[] buffer or printed to Serial.  
*******************************************************************************************/

void handle_page_0_read(int line)
{
    switch (line)
    {
    case SYSTEM_PERMANENT_FAULT_LINE:
        I2C_Array_Tx[0] = eeprom[P_FAULT_SYSTEM];   //Save balance FET state to I2C buffer -No MSB/LSB to translate
        break;

    case SYSTEM_STATE_LINE:
        I2C_Array_Tx[0] = (system_state);           //Save balance FET state to I2C buffer -No MSB/LSB to translate
        break;

    case SYSTEM_LEVEL_WARNING_LINE:
        I2C_Array_Tx[0] = (system_warnings);          //Save balance FET state to I2C buffer -No MSB/LSB to translate
        break;

    case SYSTEM_LEVEL_FAULT_LINE:
        I2C_Array_Tx[0] = (system_faults);            //Save balance FET state to I2C buffer -No MSB/LSB to translate
        break;
    }
}



void handle_rest_of_pages(int line, int page)
{
    if (line >= CELL_00_VOLTAGE_LINE && line <= CELL_10_VOLTAGE_LINE)           //Cell Voltage memory area
    {
        int voltage = bms_ic[page - 1].cells.c_codes[line] * 0.1;
        I2C_Array_Tx[0] = voltage & 0xFF;                                       //Save Voltage LSB to I2C buffer
        I2C_Array_Tx[1] = voltage >> 8;                                         //Bit shift down by 8 and save Voltage MSB to I2C Buffer
        UART_Printf("Voltage (mV): %f\n", bms_ic[page - 1].cells.c_codes[line] * 0.1); //Print memory area to serial
    }
    
    else if (line >= THERMISTOR_00_LINE && line <= THERMISTOR_36_LINE)          //Thermistor memory area
    {
        int therm = secondary_therm[page - 1][line - THERMISTOR_00_LINE];
        I2C_Array_Tx[0] = therm & 0xFF;                                         //Save Therm LSB to I2C buffer
        I2C_Array_Tx[1] = therm >> 8;                                           //Bit shift down by 8 and save Therm MSB to I2C Buffer
        UART_Printf("Temp (0.1degC): %d\n", secondary_therm[page-1][line - THERMISTOR_00_LINE]); //Print memory area to serial
    }
        
    else if (line >= BAL_CIRCUIT_CELL_01_LINE && line <= BAL_CIRCUIT_CELL_10_LINE) //Balance operation memory area
    {
        int fet_state = balance_fet[page - 1][line - THERMISTOR_36_LINE];
        I2C_Array_Tx[0] = fet_state;                                            //Save balance FET state to I2C buffer -No MSB/LSB to translate
        UART_Printf("Cell Balance %d : %d", line - THERMISTOR_36_LINE, (int)balance_fet[page-1][line - THERMISTOR_36_LINE]);  //Print memory area to serial
    }
        
    else                                                                        //Not a recognised Page/Line number
    {
        I2C_Array_Tx[0] = 0;                                                    //Set LSB to zero
        I2C_Array_Tx[1] = 0;                                                    //Set MSB to zero
        UART_Printf("Command Out Of Range");                                 //Flag error over serial
    }
}


void read_registers(int page,int line)
{    
    if (page == 0)
    {
        handle_page_0_read(line);
    }
    else
    {
        handle_rest_of_pages(line, page);
    }
} 



/*************************************write_registers*****************************************
  -Inputs to function are memory array Page, Line and Data values used to locate the 
   requested memory array area to store the received data.
  -This section uses a decode (multiple if/else statements) to find the relevant array
   storage area for the incoming data. Different memory areas need to be processed 
   differently depending upon the type of data stored in that area. 
*******************************************************************************************/

void handle_page_0_write(int line, int data)
{
    if (line < REQUEST_SECTION_LINE)
    {
        eeprom[line] = data;                        //Save incoming EEPROM value to SRAM 'eeprom' array 
        eeprom_flag = 1;                            //Flag EEPROM update required for backup of SRAM 'eeprom' to physical EEPROM
        UART_Printf("Data Written To EEPROM: %d\n", data);                       //Serial feedback confirming write operation
    }

    else if (line == CHARGE_REQUEST_LINE)
    {
        charge_requested = data;

        if (data == 1)
        {
            discharge_requested = false;              
        }
    }

    else if (line == DISCHARGE_REQUEST_LINE)
    {
        discharge_requested = data;

        if (data == 1)
        {
            charge_requested = false;              
        }           
    }

}


void write_registers(int page, int line, int data)
{
    if (page > 0 && line == HEATER_FAN_CONTROL_LINE)//GPIO control for secondaryrs (Fan/Heater) memory location   
    {        
        fan_control[page-1] = data;            //Set GPIO2 of secondary to  logic state 'on'/'off' in the memory array  
        secondary_digital_out();                    //Command GPIO2s on/off based on 'fan_control[secondary_no] logic state
        UART_Printf("GPIO: %d\n" ,data);                 //Serial feedback confirming write operation
    }

    else if (page == 0)
    {
        handle_page_0_write(line, data);
    }

    else
    {
        UART_Printf("Command Out Of Range");     //Flag error over serial 
    }        
}   



/*************************************check_serial*****************************************
  -This function should be run frequently from the main loop to check if serial data has 
   arrived in the buffer for data handling by the BMS.
  -This section uses a decode (multiple if/else statements) to find the relevant array
   storage area for the incoming data. Different memory areas need to be processed 
   differently depending upon the type of data stored in that area.
  -This function includes 'reply' messages to be printed to the serial port, there is no 
   seperate function to handle replies as with I2C as there is no defined read request
   message. 
   e.g. Page:0:Line:53:Write:1: Will request charge.
*******************************************************************************************/

void check_serial()
{
    uint8_t page_serial;                        //Variable to store incoming page number
    uint8_t line_serial;                        //Variable to store incoming line number
    int data_serial;                            //Variable to store incoming data
  
    char string[20];
	char* read_string = string;    
    bool line_found = 0;                        //Flag to signal line number is about to be received over serial
    bool page_found = 0;                        //Flag to signal page number is about to be received over serial
    bool data_found = 0;                        //Flag to signal data is about to be received over serial
    
    while (Serial.available())  
    {
        char c = Serial.read();                 //Get one byte of data from the serial buffer

        if (c == ':') //Data is ':' delimited. Process buffer after ':' is detected (full 'read_string' has been constructed)
        {  
            if (read_string == "Page")                                  //Page command detected
            {
                UART_Printf("Page:");
                page_found = true;
            }

            else if (read_string == "Line")                             //Line command detected
            {
                UART_Printf("Line:");
                line_found = true;
            }

            else if (read_string == "Read")                             //Read request detected
            {
                UART_Printf("Value:");
                read_registers(page_serial, line_serial);               //Output requested data over serial
            }

            else if (read_string == "Write")                            //Write command detected
            {
                UART_Printf("Data:");
                data_found = true;
            }

            else if (page_found)                                        //page_found flag 'on', therefore this subsequent string is page number
            {
                page_serial = read_string.toInt();
                UART_Printf("%d", page_serial);
                page_found = false;  
            }

            else if (line_found)                                        //line_found flag 'on', therefore this subsequent string is line number
            {
                line_serial = read_string.toInt();
                UART_Printf("%d", line_serial);
                line_found = false;
            }

            else if (data_found)                                        //data_found flag 'on', therefore these subsequent string is data
            {
                data_serial = read_string.toInt();
                UART_Printf("%d", data_serial);
                data_found = false;
                write_registers(page_serial, line_serial, data_serial); //Write received data to the requested area of memory
            }

            else
            {
                UART_Printf("Command Not Recognised");               //Flag error over serial
            }

            read_string = "";                                           //Clear string for new input
        } 
        else 
        {     
            read_string += c;                                           //Add input character to String
        }
    }  
}
