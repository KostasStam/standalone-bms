#ifdef BOOTLOADER
/************************************************************************************//**
* \file         Demo/_template/Boot/led.c
* \brief        LED driver source file.
* \ingroup      Boot__template
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*   Copyright (c) 2019  by Feaser    http://www.feaser.com    All rights reserved
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* This file is part of OpenBLT. OpenBLT is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* OpenBLT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
* PURPOSE. See the GNU General Public License for more details.
*
* You have received a copy of the GNU General Public License along with OpenBLT. It 
* should be located in ".\Doc\license.html". If not, contact Feaser to obtain a copy.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "boot.h"                                /* bootloader generic header          */
#include "led.h"                                 /* module header                      */
#include "../common.h"


/****************************************************************************************
* Local data declarations
****************************************************************************************/
/** \brief Holds the desired LED blink interval time. */
//static blt_int16u ledBlinkIntervalMs;


/************************************************************************************//**
** \brief     Initializes the LED blink driver.
** \param     interval_ms Specifies the desired LED blink interval time in milliseconds.
** \return    none.
**
****************************************************************************************/
void LedBlinkInit(blt_int16u interval_ms)
{
  /* TODO ##Boot Configure the GPIO pin that the LED is connected to, as a digital output
   * and make sure the LED is turned off after this configuration.
   */
  GPIO_PinDirection(PRIMARY_LED_PIN, OUTPUT);         //PRIMARY_LED_PIN is an output 43,0x01u
  GPIO_PinWrite(PRIMARY_LED_PIN, OFF);		//Turn the LED on (LOW is the voltage level)
  /* store the interval time between LED toggles */
  //ledBlinkIntervalMs = interval_ms;
} /*** end of LedBlinkInit ***/


/************************************************************************************//**
** \brief     Task function for blinking the LED as a fixed timer interval.
** \return    none.
**
****************************************************************************************/
#define TIMEOUT	150000

void LedBlinkTask(void)
{
	static uint32_t timer = TIMEOUT;
	
	if (timer > TIMEOUT)
	{ 
		timer = 0;
		UART_Printf("Feaser Bootloader: Waiting for connection with MicroBoot host");
	}

	/* toggle the LED state */
	if (timer > (TIMEOUT/2))
	{
		/* TODO ##Boot Turn the LED on. */
		GPIO_PinWrite(PRIMARY_LED_PIN, ON);
		//_SFR_IO8(0x11) |= (1 << 3);
	}
	else
	{
		/* TODO ##Boot Turn the LED off. */
		GPIO_PinWrite(PRIMARY_LED_PIN, OFF);
		//_SFR_IO8(0x11) &= ~(1 << 3);
	}

	timer++;
	
} /*** end of LedBlinkTask ***/


/************************************************************************************//**
** \brief     Cleans up the LED blink driver. This is intended to be used upon program
**            exit.
** \return    none.
**
****************************************************************************************/
void LedBlinkExit(void)
{
  /* TODO ##Boot Turn the LED off and reset the GPIO pin configuration that was
   * configured to drive the LED.
   */
} /*** end of LedBlinkExit ***/


/*********************************** end of led.c **************************************/
#endif