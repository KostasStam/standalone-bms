#ifdef BOOTLOADER
/************************************************************************************//**
* \file         Demo/_template/Prog/timer.c
* \brief        Timer driver source file.
* \ingroup      Prog__template
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*   Copyright (c) 2019  by Feaser    http://www.feaser.com    All rights reserved
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* This file is part of OpenBLT. OpenBLT is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* OpenBLT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
* PURPOSE. See the GNU General Public License for more details.
*
* You have received a copy of the GNU General Public License along with OpenBLT. It
* should be located in ".\Doc\license.html". If not, contact Feaser to obtain a copy.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "header.h"                                    /* generic header               */
#include "../common.h"


/****************************************************************************************
* Local data declarations
****************************************************************************************/
/** \brief Local variable for storing the number of milliseconds that have elapsed since
 *         startup.
 */
static unsigned long millisecond_counter;


/************************************************************************************//**
** \brief     Initializes the timer.
** \return    none.
**
****************************************************************************************/
void TimerInit(void)
{
  /* TODO ##Prog Configure a timer peripheral such that an interrupt is generated every
   * 1 millisecond. 
   */

  /* Reset the millisecond counter value. */
  millisecond_counter = 0;
} /*** end of TimerInit ***/



/************************************************************************************//**
** \brief     Updates the millisecond timer.
** \return    none.
**
****************************************************************************************/
void TimerUpdate(void)
{
  /* TODO ##Port Check with the timer peripheral if the 1 millisecond event occured. This
   * is typically done by looking at a flag bit this is set by the timer peripheral. An
   * alternative solution would use the timer peripheral's free running counter. Just
   * keep in mind that with the latter case, you would have to store the free running
   * counter value of the last time the millisecond event occured. This you can compare
   * it with the current value of the free running counter to determine if a millisecond
   * passed.
   */

   /* Increment the millisecond counter. */
   millisecond_counter++;

} /*** end of TimerUpdate ***/



/************************************************************************************//**
** \brief     Obtains the counter value of the millisecond timer.
** \return    Current value of the millisecond timer.
**
****************************************************************************************/
unsigned long TimerGet(void)
{
  /* Read and return the tick counter value. */
  return millisecond_counter;
} /*** end of TimerGet ***/


/************************************************************************************//**
** \brief     Reset the timer by placing the timer back into it's default reset
**            configuration.
** \return    none.
**
****************************************************************************************/
void TimerReset(void)
{
  /* TODO ##Port Set the timer peripheral back into the default reset value. */
} /* end of TimerReset */


/************************************************************************************//**
** \brief     Interrupt service routine of the timer.
** \return    none.
**
****************************************************************************************/
void TimerInterrupt(void)
{
  /* Increment the millisecond counter. */
  millisecond_counter++;

  /* TODO ##Prog Clear the timer peripheral's interrupt flag and, if necessary,
   * reconfigure the timer peripheral such that another timer interrupt is generated
   * in a millisecond after the last timer interrupt event. 
   */
} /*** end of TimerInterrupt ***/


/*********************************** end of timer.c ************************************/
#endif