#ifdef BOOTLOADER
/************************************************************************************//**
* \file         Demo/_template/Boot/main.c
* \brief        Bootloader application source file.
* \ingroup      Boot__template
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*   Copyright (c) 2019  by Feaser    http://www.feaser.com    All rights reserved
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* This file is part of OpenBLT. OpenBLT is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* OpenBLT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
* PURPOSE. See the GNU General Public License for more details.
*
* You have received a copy of the GNU General Public License along with OpenBLT. It 
* should be located in ".\Doc\license.html". If not, contact Feaser to obtain a copy.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "boot.h"                                /* bootloader generic header          */
#include "can.h"
#include "../common.h"
#include <avr/boot.h>

/****************************************************************************************
* Function prototypes
****************************************************************************************/
static void Init(void);
void receive_host_can_message(CAN_FRAME *frame);


/****************************************************************************************
* Set Reset Vector address to jump to Table Vector of the bootloader
****************************************************************************************/
void __attribute(( section(".bootloader_vector") )) blreset()
{
	Indirect_jump_to(0xD000);
}

/************************************************************************************//**
** \brief     This is the entry point for the bootloader application and is called
**            by the reset interrupt vector after the C-startup routines executed.
** \return    Program return code.
**
****************************************************************************************/
int main(void)
{
	/* initialize the microcontroller */
	Init();
	
	UART_Printf("\r\n-------------bootloader--------------\r\n");
	
	/* initialize the bootloader */
	BootInit();
	
	/* forces the reset vector to stay in address 0xF000 */
	volatile bool force_vector_in_flash = 0;	
	if (force_vector_in_flash) blreset();
	
	/* start the infinite program loop */
	while (1)
	{
		/* run the bootloader task */
		BootTask(); //FIXME does nothing?
	
		LedBlinkTask();
		
		interruptHandler();
		
		TimerUpdate();
	}

	/* program should never get here */
	return 0;
} /*** end of main ***/


/************************************************************************************//**
** \brief     Initializes the microcontroller.
** \return    none.
**
****************************************************************************************/
static void Init(void)
{
	/* TODO ##Boot Initialize the microcontroller. This typically consists of configuring
	* the microcontroller's system clock and configuring the GPIO for the communication
	* peripheral(s) enabled in the bootloader's configuration header file "blt_conf.h".
	*/
	
	GPIO_PinDirection(PRIMARY_LED_PIN,	OUTPUT);
	GPIO_PinDirection(SWITCH_5V_PIN,	OUTPUT);
	GPIO_PinDirection(FAN_PIN,			OUTPUT);
	GPIO_PinDirection(CONTACTOR_1_PIN,	OUTPUT);
	
	GPIO_PinWrite(SWITCH_5V_PIN, ON);
	GPIO_PinWrite(FAN_PIN, ON);
	GPIO_PinWrite(CONTACTOR_1_PIN, ON);
	GPIO_PinWrite(PRIMARY_LED_PIN, ON);

	// Low level LED GPIO initialisation (to save memory)
	//_SFR_IO8(0x10) |= (1 << 3);
	//_SFR_IO8(0x11) |= (1 << 3);
  
	_can_init(CAN_BAUDRATE_500K);	// Initialise CAN port
	setNumTXBoxes(1);				// Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.
	watchFor();
	setGeneralCallback(receive_host_can_message);	//this function will get a callback for any mailbox that doesn't have a registered callback from above.
  
	uart_init(UART_BAUD_RATE);	
	
	i2c_init();
	
	cli();               // disable interrupts

} /*** end of Init ***/


/*********************************** end of main.c *************************************/
#endif