#include "common.h"

/********************************read and process data*********************************
  -This subroutine is used to analyse data stored in the main database. As a result of
  the data analysis warning/fault flags in the main database are either raised or 
  cleared.
***************************************************************************************/

void read_and_process_data(void) 
{
	static unsigned long start = 0;
	
	if ((millis() - start) > SECOND_IN_MS) // Check every second
	{
		read_cell_temps();		// Read T0-T35 thermistors into secondarysecondary_therm[secondary_no][therm no.]
		read_cell_voltages();	// Read all cell voltages into bms_ic[secondary_no].cells.c_codes[cell no.]
		
		for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)  //Cycle through all secondary boards
		{
			process_secondary_data(secondary_no);
			prepare_and_send_can_messages();
		}	
		start = millis();
	}
}



/*************************************Fault Routine*****************************************
  -This function is entered when the system detects a fault or permanent fault state. 
  -The primary action is to switch outputs off to prevent further damage to the battery pack.
   Once all outputs have been switched off, the function monitors all input variables in order
   to detect when/if the battery is in a safe state to be switched back on.  
  -Digital output pins remain under primary control and can remain in an 'on' state during 
   a fault condition. This is useful for example to cool an over temperature battery pack
***************************************************************************************/

void fault_state(void) 
{
	UART_Printf("Fault State");
	
	system_state =  FAULT_MASK;
  
	contactors_off();
}



/*************************************charge*****************************************
  -The charge routine is highly application specific and as such will need tailoring 
  to suit the customer need. The battery will remain in the charge state once entered
  until either charge is terminated of a fault is triggered.  
***************************************************************************************/

void charge_state(void)
{
	UART_Printf("Charging State");
	
	system_state = system_state | CHARGE_CC_MASK;					//Turn on Charge
	system_state = system_state & !(STANDBY_MASK & DISCHARGE_MASK);	//Turn off Standby and Discharge
	
	contactor_control(1, ON);
	
	//charge_command(); //FIXME adjust to the charger used (CANBus commands)
	
	//balance();
	
	//read_balance_feedback();	//Read balance feedback into balancingFET[secondary_no][0]
}



/*************************************discharge*****************************************
  -The entry to the discharge routine is application specific and as such will need 
  tailoring to suit the customer need. The battery will remain in the discharge state 
  once entered until either discharge is terminated of a fault is triggered.   
***************************************************************************************/

void discharge_state(void)
{
	UART_Printf("Discharge State");
	
	system_state = system_state | DISCHARGE_MASK;                   //Turn on Discharge
	system_state = system_state & !(STANDBY_MASK & CHARGE_CC_MASK); //Turn off Standby and Charge
	
	contactor_control(1, ON);
  
	//balance();
	
	//read_balance_feedback();	//Read balance feedback into balancingFET[secondary_no][0]
}



/*************************************standby*****************************************
  -Standby is the default BMS state and is returned to once charge, discharge of fault
  states are cleared. It is important to leave contactors in an ‘Off’ state in many 
  applications, but this is application specific. Balancing is usually permitted in a 
  standby state as this is the likely state after charge termination. Secondary GPIOs are 
  required to operate during standby as a battery pack in some applications may still 
  need heating/cooling during this state.
***************************************************************************************/

void standby_state(void) 
{  
    system_state = STANDBY_MASK;	//Turn on Standby
	
    contactors_off();
	
    //balance();
	
	//read_balance_feedback();	//Read balance feedback into balancingFET[secondary_no][0]
}
