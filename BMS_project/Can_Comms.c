#ifdef APPLICATION

//TODO create CAN message definitions header file
//TODO create CAN message fillup function

#include "common.h"

#define STANDARD	0
#define EXTENDED	1
#define DATA_LENGTH 8

//**********************************************************
// Standard Message IDs
#define CONFIGURATION_REQUEST				0X100
#define CONFIGURATION_RESPONSE				0X101
#define BATTERY_STATUS						0x102
#define BATTERY_ID							0X103
#define BATTERY_INFORMATION					0X104
#define BATTERY_FUEL_GAUGE					0X105
#define CELL_VOLTAGES_A						0X106
#define CELL_VOLTAGES_B						0X107
#define CELL_VOLTAGES_C						0X108
#define CELL_VOLTAGES_D						0X109
#define CELL_VOLTAGES_E						0X10A
#define CELL_TEMPERATURES_A					0X10B
#define CELL_TEMPERATURES_B					0X10C
#define CELL_TEMPERATURES_C					0X10D
#define CELL_TEMPERATURES_D					0X10E
#define CELL_TEMPERATURES_E					0X10F
#define CELL_TEMPERATURES_F					0X110
#define CELL_TEMPERATURES_G					0X111
#define CLEAR_PERMANENT_FAULTS				0X200
#define CHANGE_CAN_SPEED_500K				0X201
#define CHANGE_CAN_SPEED_1M					0X202
#define FIRMWARE_UPDATE_REQUEST				0X667
// Extended Message IDs
#define FIRMWARE_EXT_UPDATE_REQUEST			0xC7080667

// KEY required to access configuration
#define CONFIGURATION_WRITE_KEY				0x6C16


//**********************************************************

void my_can_init(long baud)
{
	_can_init(baud);							// Initialise CAN port. must be before Serial.begincan_init(CAN_BPS_250K);
	setNumTXBoxes(1);							// Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.
	watchFor();//0x100, 0x7FF);					//Alternative call - using different syntax (should setup MOb 4)
	setGeneralCallback(receive_can_message);	//this function will get a callback for any mailbox that doesn't have a registered callback from above.
	// Take note, if a CAN message arrives for a callback, WHILE the prior-callback is still processing,
	// this GeneralCallBack will be invoked.  Watch for this in high-traffic networks...
}

void canbus_debug(char* msg)
{
	uint8_t len = strlen(msg);
	uint8_t byte[8];
	
	for (int i=0; i<len; i++)
	{
		byte[i] = msg[i];
	}
	
	send_can_message(0x0, STANDARD, len, &byte);
}

void canbus_debug_int(uint8_t addr, int64_t val)
{
	uint8_t len = sizeof(val);	
	uint8_t byte[8];
	
	for (int i=0; i<len; i++)
	{
		byte[len-1-i] = val >> (8*i);
	}
	
	send_can_message(addr, STANDARD, len, &byte);
}

void canbus_debug_float(uint8_t addr, double val)
{
	union d_to_ui convert;
	uint8_t byte[8];
	
	convert.d = val;
	
	for (int i=0; i<8; i++)
	{
		byte[7-i] = convert.ui[i];
	}
	
	send_can_message(addr, STANDARD, DATA_LENGTH, &byte);
}

uint16_t get_command(CAN_FRAME *frame)
{
	uint16_t command = (frame->data.bytes[0] << 8) +
						frame->data.bytes[1];
	#ifdef DEBUG
		UART_Printf("command: %x", command);
	#endif
	return command;
}

uint16_t get_write_key(CAN_FRAME *frame)
{
	uint16_t key = (frame->data.bytes[2] << 8) +
	frame->data.bytes[3];
	#ifdef DEBUG
	UART_Printf("write key: %x", key);
	#endif
	return key;
}

uint16_t get_data(CAN_FRAME *frame)
{
	uint16_t data = (frame->data.bytes[4] << 8) +
					(frame->data.bytes[5]);
	#ifdef DEBUG
		UART_Printf("data: %x", data);
	#endif				 
	return data;
}

bool is_configuration_key_correct(CAN_FRAME *frame)
{
	if (CONFIGURATION_WRITE_KEY == get_write_key(frame))
	{
	#ifdef DEBUG
		UART_Printf("CONFIGURATION KEY CORRECT");
	#endif
		return true;
	}
	else
	{
	#ifdef DEBUG
		UART_Printf("CONFIGURATION KEY INCORRECT");
	#endif
		return false;
	}
}

void process_configuration(uint8_t *byte, uint16_t command, uint16_t data)
{
#ifdef DEBUG
	print_config();
#endif
	
	UART_Printf("command: %x", command);
	if (command >= 0x1000)
	{
		write_single_config(command, data);
	}
		
	uint16_t config = 0;
	config = read_single_config(command);
	byte[5] = (config     ) & 0xFF;
	byte[4] = (config >> 8) & 0xFF;
	
#ifdef DEBUG
	print_config();
#endif
}

void prepare_success_response(CAN_FRAME *frame, uint8_t *data)
{
	data[0] = 0xFF;                     //Pass code correct
	data[1] = 0xFF;                     //Pass code correct

	data[2] = (frame->data.bytes[0]);   //Parrot command received
	data[3] = (frame->data.bytes[1]);   //Parrot command received
}

void prepare_failed_response(CAN_FRAME *frame, uint8_t *data)
{
	data[0] = 00;                       //Pass code incorrect
	data[1] = 00;                       //Pass code incorrect

	data[2] = (frame->data.bytes[0]);   //Parrot command received
	data[3] = (frame->data.bytes[1]);   //Parrot command received
			
	data[4] = 0;                        //Return 0
	data[5] = 0;                        //Return 0			
	data[6] = 0;                        //Return 0
	data[7] = 0;                        //Return 0
}



void receive_can_message(CAN_FRAME *frame)
{
#ifdef DEBUG
	UART_Printf("CAN ID: %X", frame->id);
#endif

	uint8_t byte[DATA_LENGTH];
	
	if (CONFIGURATION_REQUEST == frame->id)
	{
		if (is_configuration_key_correct(frame))
		{
			uint16_t command = get_command(frame);
			uint16_t data	 = get_data   (frame);
			process_configuration(byte, command, data);
			
			prepare_success_response(frame, byte);
		}
		else
		{
			prepare_failed_response(frame, byte);
		}

		send_can_message(CONFIGURATION_RESPONSE, STANDARD, DATA_LENGTH, &byte);
	}
	
	if ((FIRMWARE_UPDATE_REQUEST == frame->id) || (FIRMWARE_EXT_UPDATE_REQUEST == frame->id))
	{
		static int count_down = 5;
		UART_Printf("Firmware Update Request Detected! Count Down = %d", count_down);
		count_down--;
		
		if (count_down <=0)
		{	
			cli();               // disable interrupts
			
			UART_Printf("Rebooting to address 0xD000...");
			
			Indirect_jump_to(0xD000);
		}
	}
	
	if (CLEAR_PERMANENT_FAULTS == frame->id)
	{
		system_per_faults = 0;
		UART_Printf("System Permanent Faults are Cleared");
	}
	
	if (CHANGE_CAN_SPEED_500K == frame->id)
	{
		Can_disable();
		my_can_init(CAN_BAUDRATE_500K);	// Init CAN port
		UART_Printf("CANBus Baudrate set to 500kbps");
	}
	
	if (CHANGE_CAN_SPEED_1M == frame->id)
	{
		Can_disable();
		my_can_init(CAN_BAUDRATE_1M);	// Init CAN port
		UART_Printf("CANBus Baudrate set to 1Mbps");
	}
}



void prepare_and_send_can_messages(void)
{
	uint8_t byte[8];
	
	// Battery Status ----------------------------------------------------------------------------------------------
	
	// System State
	byte[0] = (system_state & 0xFF);
	
	// System Permanent Faults
	byte[1] = (system_per_faults      & 0xFF);
	byte[2] = (system_per_faults >> 8 & 0xFF);

	// System Faults
	byte[3] = (system_faults          & 0xFF);
	byte[4] = (system_faults     >> 8 & 0xFF);

	// System Warnings
	byte[5] = (system_warnings        & 0xFF);
	byte[6] = (system_warnings   >> 8 & 0xFF);

	// Battery Type
	byte[7] = BATTERY_TYPE;
	
	send_can_message(BATTERY_STATUS, STANDARD, DATA_LENGTH, &byte);
	

	// Battery ID ----------------------------------------------------------------------------------------------
/*
	//Pack Serial No
	byte[0] = (config.pack_serial_no >> 8);
	byte[1] = (config.pack_serial_no & 0xFF);

	// BMS Firmware
	byte[2] = (config.firmware_revision >> 8);
	byte[3] = (config.firmware_revision & 0xFF);

	// BMS Serial No
	byte[4] = (config.bms_serial_no >> 8);
	byte[5] = (config.bms_serial_no & 0xFF);
*/
	// Not Used
	byte[6] = 0;
	byte[7] = 0;

	send_can_message(BATTERY_ID, STANDARD, DATA_LENGTH, &byte);
	
	
	// Battery Information ----------------------------------------------------------------------------------------------

	//Pack Voltage
	byte[0] = (pack_voltage[0] >> 8);
	byte[1] = (pack_voltage[0] & 0xFF);

	// Max Temperature
	byte[2] = (maxTemperature[0] >> 8);
	byte[3] = (maxTemperature[0] & 0xFF);

	// Min Temperature
	byte[4] = (minTemperature[0] >> 8);
	byte[5] = (minTemperature[0] & 0xFF);

	// Not Used
	byte[6] = 0;
	byte[7] = 0;

	send_can_message(BATTERY_INFORMATION, STANDARD, DATA_LENGTH, &byte); 
      
	  
	// Cell Voltages ----------------------------------------------------------------------------------------------
	
	uint8_t j = 0, k = 0;
	
	for (uint8_t i=0; i<MAX_CELLS; i++)
	{
		byte[j]     = ((int) (bms_ic[0].cells.c_codes[i] * 0.1) >> 8);
		byte[j + 1] = ((int) (bms_ic[0].cells.c_codes[i] * 0.1) & 0xFF);
		j+=2;
		
		if (i >= MAX_CELLS-1)
		{
			byte[j]     = ((int) (max_cell_voltage[0] * 0.1) >> 8);
			byte[j + 1] = ((int) (max_cell_voltage[0] * 0.1) & 0xFF);
			j+=2;
			byte[j]     = ((int) (min_cell_voltage[0] * 0.1) >> 8);
			byte[j + 1] = ((int) (min_cell_voltage[0] * 0.1) & 0xFF);
			j+=2;
		}
		
		if (j > 7)
		{
			j = 0;
			send_can_message((CELL_VOLTAGES_A + k), STANDARD, sizeof(byte), &byte);
			k++;
		}
	}

	//Thermistors ----------------------------------------------------------------------------------------------

	j = 0;
	k = 0;

	for (uint8_t i=0; i<TOTAL_THERMISTORS; i++)
	{
		byte[j]     = (secondary_therm[0][i] >> 8);
		byte[j + 1] = (secondary_therm[0][i] & 0xFF);
		j+=2;
	
		if (j > 7)
		{
			j = 0;
			send_can_message((CELL_TEMPERATURES_A + k), STANDARD, sizeof(byte), &byte);
			k++;
		}
	}
}



void send_can_message(unsigned long CAN_ID, bool extendedMessage, uint8_t length, uint8_t *data)
{
#ifdef DEBUG
	//UART_Printf("Send Data\n");
#endif

	CAN_FRAME outgoing;
	outgoing.id = CAN_ID;
	outgoing.extended = extendedMessage;
	outgoing.priority = 4;              //0-15 lower is higher priority
	outgoing.length   = length;
  
	for (int i=0; i<length; i++)
	{
		outgoing.data.byte[i] = data[i];
	}    
	
	primary_switch_5v_control(ON);	// Controls CANBus transceiver power
	      
	sendFrame(outgoing);				// Take note: sendFrame() really only places the message into the outgoing queue,
										//            and will return TRUE if is succeeds.  This does NOT mean that the 
										//            message was actually  successfully sent at a later time.

	_delay_ms(100);                       //FIXME delay to give time for message to send             
	
	primary_switch_5v_control(OFF); // Controls CANBus transceiver power            
}
#endif
