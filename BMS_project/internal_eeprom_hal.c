#include "common.h"


//Array to store EEPROM values //FIXME can be moved to flash
const int eeprom[50] =
{
	0,1,            //Primary Serial Number             //Number of connected secondaries
	1,1001,         //Primary Firmware Revision         //Primary BMS Serial Number
	0,0,            //Lot Number                        //Build Date
	0,0,            //Reserved                          //Number of Secondaries
	0,0,            //Voltage Calibration               //Current Calibration
	42000,5,        //Over Voltage 1 threshold          //Over Voltage 1 time
	42500,5,        //Over Voltage 2 threshold          //Over Voltage 2 time
	43000,5,        //Over Voltage 3 threshold          //Over Voltage 3 time
	29900,5,        //Under Voltage 1 threshold         //Under Voltage 1 time
	28000,5,        //Under Voltage 2 threshold         //Under Voltage 2 time
	25000,5,        //Under Voltage 3 threshold         //Under Voltage 3 time
	5000,1000,      //Over Current 1 threshold          //Over Current 1 time
	80,20,          //Over Current 2 threshold          //Over Current 2 time
	100,300,        //Over Current 3 threshold          //Over Current 3 time
	310,5,          //Over Temperature 1 threshold      //Over Temperature 1 time
	320,5,          //Over Temperature 2 threshold      //Over Temperature 2 time
	350,5,          //Over Temperature 3 threshold      //Over Temperature 3 time
	150,5,          //Under Temperature 1 threshold     //Under Temperature 1 time
	120,5,          //Under Temperature 2 threshold     //Under Temperature 2 time
	100,5,          //Under Temperature 3 threshold     //Under Temperature 3 time
	320,20,         //PCB Balance Temperature threshold //Balance Temperature Hysteresis
	35000,34800,    //Balance Threshold Voltage On      //Balance recovery Voltage
	0,41000,        //System Permanent Fault            //End of Charge Voltage
	0,0,            //Reserved                          //Reserved
	0,0             //Reserved                          //Reserved
};



/*************************************eeprom_update*****************************************
  -Function to program all values stored in the SRAM memory array 'eeprom[]' into the uC
   EEPROM for backup during BMS power down. This function should be called if 'eeprom[]' is
   updated at any time.
  -Command 'EEPROM.update' is used as this will only write to the EEPROM if the desired
   value is different to the value already stored in the EEPROM. This command reduced the
   number of write actions to the EEPROM hence helping to extend the life of the uC. 
*******************************************************************************************/

void eeprom_update()
{
	for (int line = 0; line < TOTAL_EEPROM_LINES; line++)			//Cycle through all 50 lines of 'eeprom[]' array                          
	{
		UART_Printf("EEPROM TOTAL %d : %d\n", line, eeprom[line]);
		uint8_t eeprom_msb = ((int) (eeprom[line]) >> 8);			//Bit shift the 2byte 'eeprom[]' value down by 8 to remove LSB 
		UART_Printf(" MSB: %d\n", eeprom_msb); 
		EEPROM_WriteByte(line*2 ,eeprom_msb);						//Save the remaining MSB into EEPROM
		uint8_t eeprom_lsb = ((int) (eeprom[line]) & 0xFF);			//Remove the MSB from 2byte 'eeprom[]' to leave LSB only
		UART_Printf(" LSB: %d\n", eeprom_lsb);            
		EEPROM_WriteByte(line*2 + 1, eeprom_lsb);					//Save this remaining LSB into EEPROM
	} 
}



/************************************* eeprom_init*************************************
  -This function checks the serial number of the primary control PCB, if the serial number
   hasn't been programmed before (value of 265535), it is programmed to a new default value
   of 0. If this serial number is 265535 or a Steatite default value of 0, the BMS is 
   treated as non-configured, and all EEPROM values are configured to default voltage/current/
   temperature thresholds by function eeprom_update. 
  -If thresholds values are updated by the user, a new serial number must be programmed into
   the BMS before powering down to back-up data to EEPROM an avoid data loss.
*******************************************************************************************/

void eeprom_init() 
{ 
	//If serial number = 0 (default firmware value) or 265535 (default micro-controller value) reset EEPROM to default.
	if ((EEPROM_ReadByte(0)==0	 && EEPROM_ReadByte(1)==0) || 
		(EEPROM_ReadByte(0)==255 && EEPROM_ReadByte(1)==255))
	{
		eeprom_update();											//write eeprom SRAM array into physical EEPROM
		EEPROM_WriteByte(0, 0);										//set serial number to 0 (default EEPROM is 255 which is confusing)
		EEPROM_WriteByte(1, 0);										//set serial number to 0 (default EEPROM is 255 which is confusing)
	}  
	else															//Read EEPROM values into eeprom array in SRAM for general use/manipulation  
	{
		for (int i=0; i < TOTAL_EEPROM_LINES; i++)
		{
			//eeprom[i] = (int)EEPROM_ReadByte(i*2) << 8;			//Bit shift by 8 to form MSB. i*2 to translate line to EEPROM location
			//eeprom[i] = eeprom[i] + EEPROM_ReadByte(i*2 + 1);		//No bit shift needed for LSB. i*2+1 to translate line to EEPROM location       
		}   
	}
}

