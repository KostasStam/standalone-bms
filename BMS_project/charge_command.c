#include "common.h"

/*************************************charge*****************************************
  -The charge routine is highly application specific and as such will need tailoring 
  to suit the customer need. The battery will remain in the charge state once entered
  until either charge is terminated of a fault is triggered.  
***************************************************************************************/

void charge_command()
{
	UART_Printf("SEND REQEST CHARGE COMMAND");
	
  //Cell1
  outgoing.data.byte[1] = (secondary_therm[0][32] & 0xFF);
  outgoing.data.byte[0] = (secondary_therm[0][32] >> 8);

  //Cell2
  outgoing.data.byte[3] = (secondary_therm[0][33] & 0xFF);
  outgoing.data.byte[2] = (secondary_therm[0][33] >> 8);

  //Cell3
  outgoing.data.byte[5] = (secondary_therm[0][34] & 0xFF);
  outgoing.data.byte[4] = (secondary_therm[0][34] >> 8);

  //Cell4
  outgoing.data.byte[7] = (secondary_therm[0][35] & 0xFF);
  outgoing.data.byte[6] = (secondary_therm[0][35] >> 8);

  send_can_message(0x113, 0);


  //Start Charge (50.4V charge voltage at 25A CC)
  //Max Temperature
  outgoing.data.byte[0] = 0x01; //TODO magic numbers
  outgoing.data.byte[1] = 0xF8;
  outgoing.data.byte[2] = 0x00;
  outgoing.data.byte[3] = 0xFA;
  outgoing.data.byte[4] = 0x00;
  outgoing.data.byte[5] = 0xFF;
  outgoing.data.byte[6] = 0xFF;
  outgoing.data.byte[7] = 0xFF;
  
  send_can_message(0x1806E5F4, 1); //TODO magic numbers

  for (int secondary_no = 0; secondary_no < TOTAL_SECONDARIES; secondary_no++)        //Cycle through all secondary boards
  {    
    for (int cell_no=0; cell_no < TOTAL_CELLS; cell_no++)                             //Cycle through all secondary cells     
    {
      if (bms_ic[secondary_no].cells.c_codes[cell_no] >= eeprom[CHARGE_END_VOLTAGE])  //Charge Complete  
      { 
        //Start Charge (50.4V charge voltage)
        //Max Temperature 
        
        outgoing.data.byte[0] = 0x01; //TODO magic numbers
        outgoing.data.byte[1] = 0xF8;
        outgoing.data.byte[2] = 0x00;
        outgoing.data.byte[3] = 0x00;
        outgoing.data.byte[4] = 0x01;
        outgoing.data.byte[5] = 0xFF;
        outgoing.data.byte[6] = 0xFF;
        outgoing.data.byte[7] = 0xFF;
        
        send_can_message(0x1806E5F4, 1); //TODO magic numbers          
        charge_requested = 0;	//Charge no longer requested
      }
    }
  }
}
